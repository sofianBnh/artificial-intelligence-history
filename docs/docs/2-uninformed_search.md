# Description
<br>

The most basic type of search is the uninformed (or blind) search, it does not ues any knowledge about the
problem and there for it does not require as much work as the informed one but this also means that it searches
the state space without any guidance. <br>

This type of search can be implemented two algorithms depending on the nature of the problem:

* **Tree Search** [#](2-uninformed_search.md#tree-search)

* **Graph search** [#](2-uninformed_search.md#graph-search)

<br>

This type of problems also uses a __fringe__ which is a queue that stores the generated nodes and sorts them according
to a specific strategy [#](2-uninformed_search.md#strategies). 

<br>
---
<br>


#Search Algorithms
<br>

The search algorithms presented here are simple and use a fringe as a data structure to manipulate the nodes, they both 
return either a solution or a failure (None).

<br>

##Tree Search
<br>

Tree search is kind of search that runs through a tree shaped state space.

```python
def tree_search(problem: Problem, fringe: Queue) -> bool or list:
    """
    The blind search of the problem
    :param problem: Problem
    :param fringe: Queue
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(Node(problem.initial_state, None, None, 0, 0))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend(node, problem))
```


<br>

##Graph Search
<br>

Graph search is kind of search that runs through a graph shaped state space,the difference between this search and the 
tree search is that we are keeping a list of visited nodes the __blacklist__ in the code.

```python
def graph_search(problem: Problem, fringe: Queue) -> bool or list:
    """
    The blind search of the problem
    :param problem: Problem
    :param fringe: Queue
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(Node(problem.initial_state, None, None, 0, 0))

    # List of the already visited states
    blacklist = []
    
    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        if node.state not in blacklist:
            blacklist.append(node.state)
            fringe.insert_all(expend(node, problem))

```

<br>

##Strategies
<br>

Strategies are the way we manage the queue of nodes (the fringe), this includes the way we store them, insert them and
how we get the next element.

All these strategies are based on the abstract class **Queue** from the main `core.py` file [#](1-core.md#queue).

<br>

+ **BFS** : Inserts elements in a _first in first out_ way, making the search layer by layer.

```python
class BFSQueue(Queue):
    def insert(self, element):
        self.elements.append(element)
```

<br>

+ **DFS** : Inserts elements in a _last in first out_ way, making the search branch by branch.

```python
class DFSQueue(Queue):
    def insert(self, element):
        self.elements.insert(0, element)
```

<br>

+ **DLS** : Based on DFS but with a limitation in depth.

```python
class DLSQueue(Queue):
    def __init__(self, max_depth):
        super().__init__()
        self.max_depth = max_depth

    def insert(self, element):
        if element.depth < self.max_depth:
            self.elements.insert(0, element)
```

<br>

+ **IDS** : Iteratively uses DLS an increments the max depth with each iteration.

```python
class IDSQueue(Queue):
    def __init__(self):
        super().__init__()
        self.depth = 0
        self.head = None

    def insert(self, element):
        if element.depth <= self.depth:
            if self.head is None:
                self.head = element
            self.elements.insert(0, element)

    def remove_first(self):
        elem = super().remove_first()
        if len(self.elements) == 0:
            self.elements.append(self.head)
            self.depth += 1
        return elem

```

<br>

+ **UCS** : Sorts the nodes by their cost from the smalled to the highest.

```python
class UCSQueue(Queue):
    def __init__(self):
        super().__init__()
        self.visited = []

    def insert(self, element):
        if element.state not in self.visited:
            self.elements.append(element)
            self.elements.sort(key=lambda x: x.cost)

    def remove_first(self):
        element = super(UCSQueue, self).remove_first()
        self.visited.append(element.state)
        return element

```

<br>
---
<br>


# Test Case
<br>

To test this algorithm, we are using the maze exploration problem.
The agent needs to find the way out of the maze starting at a certain point
and having the possibility to move to the adjacent places.

<br>

**The Maze**
<br>

The maze is represented by a matrix (in this case a 6 x 7 matrix ) of characters.

* **Map** 
                                      
                                      
                                      | U | X |   |   | X |   |   |   
                                      |   | X |   |   |   |   |   |   
                                      |   | X |   | X |   | X |   |   
                                      |   | X | X |   |   | X |   |   
                                      |   |   |   |   | X |   |   |   
                                      |   | X |   |   | X | O | X |   
<br>

Mapping:

* **X** : Representing Walls.

* **U** : Representing the starting point.

* **O** : Representing the door (way out).

* **_Spaces_** : Representing the road.

* ** \* **  : Representing the best path.

<br>


##Search Strategy
<br>

In this example we are using the **BFS** strategy to search the road because:

* Each step has the same cost.

* We need to keep all nodes in the memory in case of a dead end.

* Returns the shortest path.

<br> 

And a **Graph Search** because:

* We could face the repeated states problem.

<br>


##Code 
<br>

The main code for this test case is structured as follows:

1. Creating the maze.

2. Creating the initial and the goal states.

3. Creating the Problem instance using these both states.

4. Launching the search.

5. Retrieving the results and displaying them.


```python
    def run():
        # Creating maze
        test_maze = [
            [" ", "X", " ", " ", " ", " ", " "],
            [" ", "X", " ", "X", " ", "X", " "],
            [" ", "X", "X", " ", " ", "X", " "],
            [" ", " ", " ", " ", "X", " ", " "],
            [" ", "X", " ", " ", "X", " ", "X"]
        ]
        # Initialize the problem :
    
        # -  Create the initial state
        initial_state = MazeState(test_maze, (0, 0))
    
        # -  Create goal state function
        goal_state = MazeState(test_maze, (4, 5))
    
        # - Create the problem object
        problem = MazeExplorationProblem(initial_state, goal_state)
    
        # Run the tree search :
        result = graph_search(problem, BFSQueue())
    
        # Print result
        if result:
            result.reverse()
            print("The number of steps to the goal state : ", len(result))
            print()
            print("The steps are : \n")
            i = 1
            for res in result:
                print("- ", res)
                i += 1
        else:
            print("No solutions")
```

<br>

###State Representation
<br>

We represent the state using the position in the maze and the maze itself.
The state is valid if the coordinates point to an empty place in the maze.

```python
    class MazeState(State):

        def __init__(self, maze: list, position: tuple):
            """
            Maze state initialization
            :param maze: matrix representing the map using spaces for roads
            :param position: initial position (x,y)
            """
            self.maze = maze
            self.position = position
    
        @staticmethod
        def is_valid_state(state) -> bool:
            """
            :param state: state to be tested
            :returns: if the current position is valid
            """
            if 0 <= state.position[0] < len(state.maze) \
                    and 0 <= state.position[1] < len(state.maze[0]):
                return state.maze[state.position[0]][state.position[1]] == " "
            return False
    
        @staticmethod
        def get_copy(state):
            """
            :returns: a copy of the state
            """
            return MazeState(copy.deepcopy(state.maze),
                             copy.deepcopy(state.position))
    
        def __eq__(self, state) -> bool:
            return self.position == state.position
    
        def __str__(self):
            return "( {} , {} )".format(self.position[0], self.position[1])


```

<br>

###Path cost function
<br>

Since in this case the cost of all the actions is the same we use the predefined **basic cost funciton** 
[#](1-core.md#predefined-functions)

<br>

###Successor function
<br>

The possible moves for the agent would be the four directions north, south, west and east and each of these directions 
has a translation for x and w values. We stored these translations in the __cases__ list the iterate on each one of the 
and see if the state is valid then add it to the results.
 
```python
    def f_successor(self, state: MazeState) -> list:
        # possible transitions
        cases = {
            (0, 1): "East",
            (0, -1): "West",
            (1, 0): "South",
            (-1, 0): "North"
        }

        # testing each direction if the state is valid we add it to the results
        results = []
        for tuples, action in cases.items():
            current_state = move(state, *tuples)
            if MazeState.is_valid_state(current_state):
                results.append((action, current_state))

        return results

```
 
<br>

To test the new state with the translation we use the __move__ function which returns a new state with the updated 
coordinates.

```python
   def move(state: MazeState, translation_x: int, translation_y: int) -> MazeState:
       """
       Function that creates a new state with a position translated by the parameters
       :param state: initial state
       :param translation_x: translation in the X axe
       :param translation_y: translation in the Y axe
       :returns: a new state with the translated position
       """
       new_x = state.position[0] + translation_x
       new_y = state.position[1] + translation_y
   
       new_state = state.get_copy(state)
       new_state.position = (new_x, new_y)
   
       return new_state
```
<br>

###Goal Test function
<br>

The goal test in this case is the ending point of the maze with the coordinates **(4,5)**. The goal test function is then
a test of the equality of the current state with the state representing this position. 

<br>

##Results
<br>

The algorithm outputted after the search : 

    The number of steps to the goal state :  17
    
    The steps are : 
    
    -  South
    -  South
    -  South
    -  East
    -  East
    -  East
    -  North
    -  East
    -  North
    -  North
    -  East
    -  East
    -  South
    -  South
    -  South
    -  West
    -  South
    

<br>   
 
<button type="button" class="btn btn-primary btn-sm" onclick="resolve()" id="button">Resolve</button>
<button type="button" class="btn btn-primary btn-sm" onclick="next()" id="button1" disabled="disabled"><i class="fa fa-play"></i></button>

                                     