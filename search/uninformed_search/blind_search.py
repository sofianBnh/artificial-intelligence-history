"""
Blind search algorithms
"""

from .core import *


# Searches:
def tree_search(problem: Problem, fringe: Queue) -> bool or list:
    """
    The blind search of the problem
    :param problem: Problem
    :param fringe: Queue
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(Node(problem.initial_state, None, None, 0, 0))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend(node, problem))


def graph_search(problem: Problem, fringe: Queue) -> bool or list:
    """
    The blind search of the problem
    :param problem: Problem
    :param fringe: Queue
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(Node(problem.initial_state, None, None, 0, 0))

    # List of the already visited states
    blacklist = []
    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        if node.state not in blacklist:
            blacklist.append(node.state)
            fringe.insert_all(expend(node, problem))
