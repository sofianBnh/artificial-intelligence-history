"""
Local Search algorithms
"""
import copy

from .core import *


def simulated_annealing(problem: LocalSearchProblem, schedule: Temperature, k_factor: float = 1):
    """
    Simulated annealing algorithm
    :param k_factor: bosyman factor
    :param problem: Local Search Problem with a neighbor function implemented
    :param schedule: a Temperature scheduler
    :returns the best solution found
    """
    current = copy.deepcopy(problem.initial_state)
    schedule.initialize_temperature()

    while not schedule.is_min_temperature():
        neighbor = problem.neighbour(current)

        if neighbor.is_valid_state(neighbor):
            if acceptance_probability(current.quality(), neighbor.quality(),
                                      schedule.get_current_temperature(), k_factor):
                current = neighbor

        schedule.update_temperature()

    return current

