"""
Graph coloration example
"""

from search.constraint_satisfaction_problems.csp_resolution import *


def run():
    # Available colors
    colors = ["red", "blue", "yellow"]

    # Variables and domains definition
    domains = {
        "x1": copy.deepcopy(colors),
        "x2": copy.deepcopy(colors),
        "x3": copy.deepcopy(colors),
        "x4": copy.deepcopy(colors)
    }

    # Constraints
    constraints = [
        Constraint(["x1", "x2"], lambda assignments: assignments["x1"] != assignments["x2"]),
        Constraint(["x1", "x3"], lambda assignments: assignments["x1"] != assignments["x3"]),
        Constraint(["x2", "x3"], lambda assignments: assignments["x2"] != assignments["x3"]),
        Constraint(["x2", "x4"], lambda assignments: assignments["x2"] != assignments["x4"]),
    ]

    # Initialization of the problem
    problem = CSProblem(domains, constraints)

    # Launch backtracking search
    back_tracking_search(problem, most_constraining_variable, least_constraining_value, True)

    # Print results
    print("Result assignments :")
    for assignment, value in problem.assignments.items():
        print("- {} = {} ".format(assignment, value))


if __name__ == '__main__':
    run()
