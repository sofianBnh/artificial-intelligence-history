"""
Genetic algorithms
"""

from .core import *


def genetic_search(problem: GeneticProblem, population_size: int,
                   mutation_probability: float) -> ChromosomeState:
    """
    Performs a genetic search with customisable options
    :param problem: Genetic Problem
    :param population_size: number of members in a population
    :param mutation_probability: decides when to make the mutation
    :return: the best possible member
    """
    gen = 0
    population = []
    new_population = []
    best = problem.initial_state
    best_fitness = problem.fitness(best)
    elite = [best_fitness]

    for i in range(population_size):
        population.append(problem.generate_random_state())

    while not problem.stop_generation(best_fitness=elite, gen_count=gen):

        for i in range(int(population_size / 2 - 1)):

            parent_x = problem.get_parent_form_population(population)

            parent_y = problem.get_parent_form_population(population)

            children = problem.reproduce(parent_x, parent_y)

            for child in children:
                # if a random number from 0 to 1 is less than mutation probability
                if uniform(0, 1) < mutation_probability:
                    child = problem.mutate(child)

                new_population.append(child)
                current_fitness = problem.fitness(child)

                if current_fitness > best_fitness:
                    best = copy.deepcopy(child)
                    best_fitness = current_fitness
                    elite.append(best_fitness)

        population += new_population
        population = problem.selection(population, population_size)
        new_population = []
        gen += 1

    return best
