"""
Dependencies for Constraint Satisfaction Problems
"""

import copy


# Beans
class Constraint:
    def __init__(self, variables: list, expression):
        """
        Representation of constraints
        :param variables: list of variable names as str
        :param expression: lambda expression returning the constraint
        """
        self.variables = variables
        self.expression = expression


# Problem
class CSProblem:
    def __init__(self, domains: dict, constraints: list, assignments: dict = None):
        """
        Model for the CSPs
        :param domains: dictionary of variables : < "name of variable" : list of possible values  >.
        :param constraints: list of Constraints.
        :param assignments: dict of assignments : < "name of variable" : value > ,
               default empty dict.
        """
        if assignments is None:
            assignments = {}

        self.domains = domains
        self.constraints = constraints
        self.assignments = assignments

    def is_complete(self) -> bool:
        """
        checks for completeness by comparing the number of assignments to
        the number of variables.
        :return: if it's complete or not
        """
        return len(self.domains) == len(self.assignments)

    def is_consistent(self, assignment) -> bool:
        """
        checks the coherence of the current assignments
        :return: if the current assignments are coherent with the conditions
        """
        for constraint in self.constraints:

            skip = False
            for var in constraint.variables:
                # If the variable isn't assigned yet don't skip the constraint
                if var not in assignment.keys():
                    skip = True

            if not skip:
                if not constraint.expression(assignment):
                    return False

        return True

    def is_assignment_consistent(self, variable: str, value) -> bool:
        """
        tests the consistent of an assignment ( variable , value )
        without saving it.

        :param variable: name of the variable
        :param value: value of variable
        :return: if the assignment is coherent or not
        """
        test_assignment = copy.deepcopy(self.assignments)
        test_assignment[variable] = value
        return self.is_consistent(test_assignment)

    def set_assignment(self, variable: str, value):
        """
        sets an assignment in the current assignments list
        :param variable: name of the variable
        :param value: value of the variable
        """
        self.assignments[variable] = value

    def remove_assignment(self, variable: str):
        """
        Removes an assignment from the current assignments
        :param variable: variable name
        """
        del self.assignments[variable]

    def __str__(self):
        return "Domains : " + str(self.domains) + " \n" \
               + "Assignments :" + str(self.assignments)

    # Heuristics


# Variable selection
def ordered_variables(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the first alphabetically unassigned variable
    """
    possible_vars = _get_remaining_variables(problem)

    possible_vars.sort()
    return possible_vars[0]


def minimum_remaining_values(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the variable having the fewest possible values
    """
    variables: list = _get_remaining_variables(problem)

    mrv = variables.pop(0)
    dom = problem.domains[mrv]

    minimum_values_length = len(dom)

    for var in variables:
        # if number of remaining values in the domain are less than mvl
        if len(problem.domains[var]) < minimum_values_length:
            mrv = var

    return mrv


def most_constraining_variable(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the variable that is in the biggest number of constraints
    """
    count = {}
    unassigned = _get_remaining_variables(problem)

    # counting occurrences
    for constraint in problem.constraints:
        for var in constraint.variables:
            if var in unassigned:
                if var not in count.keys():
                    count[var] = 1
                else:
                    count[var] += 1

    # finding the max
    mcv_count = max(count.values())
    for var, counted in count.items():
        if counted == mcv_count:
            return var


# value selection
def ordered_values(problem: CSProblem, variable: str) -> list:
    """
    :param problem: CSP with current assignments
    :param variable: variable name
    :returns the values for the variable ordered in an alphabetical way
    """
    problem.domains[variable].sort()
    return problem.domains[variable]


def least_constraining_value(problem: CSProblem, variable: str) -> list:
    """
    :param problem: CSP with current assignments
    :param variable: variable name
    :returns the values ordered by the number of domains affected
    """
    order = {}
    values = problem.domains[variable]
    # try
    initial_csp = copy.deepcopy(problem)
    # counting phase
    for val in values:
        problem.set_assignment(variable, val)

        order[val] = _inconsistency_count(problem, variable)

        problem = copy.deepcopy(initial_csp)

    return sorted(order, key=lambda x: order[x])


# updating domains
def forward_checking(problem: CSProblem, variable: str):
    """
    :param variable: variable lastly assigned
    :param problem: problem: CSP with current assignments
    :returns the csp modified with new domains and the list of values for the variable
    """
    new_domains = copy.deepcopy(problem.domains)

    neighbors = set(_get_neighbors(problem, variable))
    unassigned = set(_get_remaining_variables(problem))

    tested = unassigned.intersection(neighbors)

    for neighbor in tested:
        for value in new_domains[neighbor]:
            if not problem.is_assignment_consistent(neighbor, value):
                new_domains[neighbor].remove(value)

    problem.domains = new_domains


# private methods
def _get_remaining_variables(problem: CSProblem):
    """
    :param problem: csp problem
    :returns: the non assigned variables
    """
    variables = list(problem.domains.keys())
    assigned_variables = list(problem.assignments.keys())

    possible_vars = []

    for var in variables:
        if var not in assigned_variables:
            possible_vars.append(var)

    return possible_vars


def _get_neighbors(problem: CSProblem, variable: str) -> list:
    """
    :param problem: csp problem
    :param variable: variable from the csp variables
    :returns: the neighbour variables in the constraints graph
    """
    neighbors = set()

    for constraint in problem.constraints:

        if variable in constraint.variables:

            for var in constraint.variables:
                neighbors.add(var)

    neighbors.remove(variable)
    return list(neighbors)


def _inconsistency_count(problem: CSProblem, variable: str) -> int:
    """
    :param problem: csp problem
    :param variable: variable from csp variables
    :returns: the number of inconsistencies for each value from the domain of tha variable
    """
    count = 0
    # for each neighbor check how many values don't work and sum
    for neighbor in _get_neighbors(problem, variable):
        for value in problem.domains[neighbor]:
            if not problem.is_assignment_consistent(neighbor, value):
                count += 1
    return count
