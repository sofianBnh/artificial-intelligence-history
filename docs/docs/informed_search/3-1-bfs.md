# Description
<br>

This kind of search in contrast with the uninformed search [#](../2-uninformed_search.md) uses some knowledge from the
problem it self to help guide the search in the direction of the goal. 

<br>

This knowledge is injected into the search algorithms via the __heuristic function__ [#](3-1-bfs.md#heuristics), 
this function returns for each state an approximation of the distance between it and the goal state this information 
(usually called hint) is used to sort the nodes in the fringe. 

<br>

This hint is used in different ways by different algorithms, for example :

* **Greedy Search** : Uses the hint as it is to sort the nodes.

* **A Star** : Uses the hint plus the cost of the node to have a more global estimation of the distance.

<br>
---
<br>


#Search Algorithms
<br>

These two search algorithms use a kind of nodes that has in addition of the usual information of generic node 
[#](../1-core.md#Node) a hint attribute. 

<br>

**Informed Node**

```python
class InformedNode(Node):
    """
    Node that contains a special attribute "hint"
    """
    def __init__(self, state: State, parent, action: str or None, depth: int,
                 cost: int, hint: int):
        super().__init__(state, parent, action, depth, cost)
        self.hint = hint
```

<br>

This attribute is used in a spacial kind of queue, the _greedy queue_, this queue sorts
the nodes according to the hint parameter which is set in the expansion function. 

<br>

**Greedy Queue**

```python
class GreedyQueue(Queue):
    """
    Queue that sorts the nodes according to the hint
    handles the repeated states
    """

    def __init__(self):
        super().__init__()
        self.visited = []

    def insert(self, element):
        if element.state not in self.visited:
            self.elements.append(element)
            self.elements.sort(key=operator.attrgetter('hint'))

    def remove_first(self):
        element = super(GreedyQueue, self).remove_first()
        self.visited.append(element.state)
        return element

```

<br>

Both these algorithms use a similar skeleton to the blind search but differ in the expansion function.

<br>

##Greedy Search
<br>

Greedy search focuses on the best next node according to the heuristic's output only which makes it very local 
and sometimes not optimal.

<br>

**Main Algorithm**

```python
def greedy_search(problem: InformedProblem, fringe: GreedyQueue):
    """
    The greedy search of the problem
    :param problem: Problem containing an heuristic function
    :param fringe: Queue compatible with sorting by hint
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(InformedNode(
        problem.initial_state,
        None, None, 0, 0,
        problem.heuristic(problem.initial_state)))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend_local(node, problem))

```

<br>

**Expansion Function**

```python
def expend_local(node: InformedNode, problem: InformedProblem) -> list:
    """
    Returns the list of nodes that expend the entry node
    :param node: node to be expanded
    :param problem: data for expansion
    :return: list : of Nodes with the local hint approximation
    """
    successors = []

    for action, result in problem.f_successor(node.state):
        path_cost = node.cost + problem.path_cost(node.state, action, result)
        depth = node.depth + 1
        hint = problem.heuristic(result)
        successors.append(InformedNode(result, node, action, depth, path_cost, hint))

    return successors

```


<br>

##A Star
<br>


A star as opposed to greedy search, does not just focus on the heuristic's output only but also relies on the cost or 
the actual previously calculated distance from the root to the current node, which makes the approximation more accurate 
and global by taking in consideration the previous steps into account and therefor if the heuristic is coherent makes a 
star complete and optimal.

<br>
 
**Main Algorithm**

```python
def a_star(problem: InformedProblem, fringe: GreedyQueue):
    """
     The a* search of the problem
     :param problem: Problem containing an heuristic function
     :param fringe: Queue compatible with sorting by hint
     :return: False if there is no solution or the list of the actions (names)
     """

    fringe.insert(InformedNode(
        problem.initial_state,
        None, None, 0, 0,
        problem.heuristic(problem.initial_state)))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend_global(node, problem))

```

<br>

**Expansion Function**

```python
def expend_global(node: InformedNode, problem: InformedProblem) -> list:
    """
    Returns the list of nodes that expend the entry node
    :param node: node to be expanded
    :param problem: data for expansion
    :return: list : of Nodes  with the global hint approximation
    """
    successors = []

    for action, result in problem.f_successor(node.state):
        path_cost = node.cost + problem.path_cost(node.state, action, result)
        depth = node.depth + 1
        # the difference here
        hint = path_cost + problem.heuristic(result)
        successors.append(InformedNode(result, node, action, depth, path_cost, hint))

    return successors

```

<br>
---
<br>


#Heuristics
<br>

Heuristics are functions used by the developer of an intelligent system to add the "intelligence" to the system by 
incorporating some kind f useful information to the blind search algorithms, and making them this way more efficient and
therefor more optimized and make the convergence to a solution faster. These functions are usually made by looking at a 
relaxation of the problem and focusing on the overall information we could use.

<br>

Some examples of known heuristics in distance approximation and space exploitation:

* Manhattan distance.

* Euclidean distance.

* Beeline. 

<br>

Heuristics are problem dependant functions which can make them sometimes hard to implement.

<br>
---
<br>


# Test Case
<br>

To test this algorithm, we are using the maze exploration problem, 
the same as the one from the uninformed search [#](../2-uninformed_search.md). <br>

The agent needs to find the way out of the maze starting at a certain point
and having the possibility to move to the adjacent places.

<br>

**The Maze**
<br>

The maze is represented by a matrix (in this case a 6 x 7 matrix ) of characters.

* **Map** 
                                      
                                      
                                      | U | X |   |   | X |   |   |   
                                      |   | X |   |   |   |   |   |   
                                      |   | X |   | X |   | X |   |   
                                      |   | X | X |   |   | X |   |   
                                      |   |   |   |   | X |   |   |   
                                      |   | X |   |   | X | O | X |   
                  
<br>
                                      
Mapping:

* **X** : Representing Walls.

* **U** : Representing the starting point.

* **O** : Representing the door (way out).

* **_Spaces_** : Representing the road.

* **\***  : Representing the best path.

<br>

##Search Strategy
<br>

For this example we use the **A star** algorithm just to demonstrate the usage of the algorithm, but in a more complete 
map greedy search may not give us the best result and we would then have to use a star.


<br>

##Code
<br>

The main code for this test case is structured as follows:

1. Creating the maze.

2. Creating the initial and the goal states.

3. Creating the Problem instance using these both states.

4. Launching the search.

5. Retrieving the results and displaying them.

```python
def run():
    global goal_state
    # Creating maze
    test_maze = [
        [" ", "X", " ", " ", " ", " ", " "],
        [" ", "X", " ", "X", " ", "X", " "],
        [" ", "X", "X", " ", " ", "X", " "],
        [" ", " ", " ", " ", "X", " ", " "],
        [" ", "X", " ", " ", "X", " ", "X"]
    ]
    # Initialize the problem :

    # -  Create the initial state
    initial_state = MazeState(test_maze, (0, 0))

    # -  Create goal state function
    goal_state = MazeState(test_maze, (4, 5))

    # - Create the problem object
    problem = MazeExplorationProblem(initial_state, goal_state)

    # Run the A star search :
    result = a_star(problem, GreedyQueue())

    # Print result
    if result:
        result.reverse()
        print("The number of steps to the goal state : ", len(result))
        print()
        print("The steps are : \n")
        i = 1
        for res in result:
            print("- ", res)
            i += 1
    else:
        print("No solutions")

```

<br>

###State Representation
<br>

We represent the state using the position in the maze and the maze itself.
The state is valid if the coordinates point to an empty place in the maze.
Same as the one used in uninformed search [#](../2-uninformed_search.md#state-representation). 

```python
class MazeState(State):

    def __init__(self, maze: list, position: tuple):
        """
        Maze state initialization
        :param maze: matrix representing the map using spaces for roads
        :param position: initial position (x,y)
        """
        self.maze = maze
        self.position = position

    @staticmethod
    def is_valid_state(state) -> bool:
        """
        :param state: state to be tested
        :returns: if the current position is valid
        """
        if 0 <= state.position[0] < len(state.maze) \ 
            and 0 <= state.position[1] < len(state.maze[0]):
            
            return state.maze[state.position[0]][state.position[1]] == " "
        return False

    @staticmethod
    def get_copy(state):
        """
        :returns: a copy of the state
        """
        return MazeState(copy.deepcopy(state.maze),
                         copy.deepcopy(state.position))

    def __eq__(self, state) -> bool:
        return self.position == state.position

    def __str__(self):
        return "( {} , {} )".format(self.position[0], self.position[1])

```

<br>

###Path Cost Function
<br>

Since in this case the cost of all the actions is the same we use the predefined **basic cost funciton** 
[#](../1-core.md#predefined-functions)

<br>

###Successor Function
<br>

The possible moves for the agent would be the four directions north, south, west and east and each of these directions 
has a translation for x and w values. We stored these translations in the __cases__ list the iterate on each one of the 
and see if the state is valid then add it to the results.

 
```python
    def f_successor(self, state: MazeState) -> list:
        # possible transitions
        cases = {
            (0, 1): "East",
            (0, -1): "West",
            (1, 0): "South",
            (-1, 0): "North"
        }

        # testing each direction if the state is valid we add it to the results
        results = []
        for tuples, action in cases.items():
            current_state = move(state, *tuples)
            if MazeState.is_valid_state(current_state):
                results.append((action, current_state))

        return results

```
 
<br>

To test the new state with the translation we use the __move__ function which returns a new state with the updated 
coordinates.

```python
   def move(state: MazeState, translation_x: int, translation_y: int) -> MazeState:
       """
       Function that creates a new state with a position translated by the parameters
       :param state: initial state
       :param translation_x: translation in the X axe
       :param translation_y: translation in the Y axe
       :returns: a new state with the translated position
       """
       new_x = state.position[0] + translation_x
       new_y = state.position[1] + translation_y
   
       new_state = state.get_copy(state)
       new_state.position = (new_x, new_y)
   
       return new_state
```
<br>

###Goal Test Function
<br>

The goal test in this case is the ending point of the maze with the coordinates **(4,5)**. The goal test function is then
a test of the equality of the current state with the state representing this position. 

<br>

###Heuristic Function
<br>

The heuristic function used here is a manhattan distance between the current state and the goal state which gives an 
approximation to the number of steps the agent has to make before getting to the goal state which is the end.

```python
    def heuristic(state: MazeState) -> int:
        """
        Heuristic calculating an approximate distance between the
        current position and the goal position
        :param state: current state
        :returns: the mahatan distance between the state and the goal
        """
        count = 0
        for j in range(2):
            count += abs(goal_state.position[j] - state.position[j])
        return count
```

<br>

##Results
<br>

The algorithm outputted after the search : 

    The number of steps to the goal state :  17
    
    The steps are : 
    
    -  South
    -  South
    -  South
    -  East
    -  East
    -  East
    -  North
    -  East
    -  North
    -  North
    -  East
    -  East
    -  South
    -  South
    -  South
    -  West
    -  South

<br>   
 
<button type="button" class="btn btn-primary btn-sm" onclick="resolve()" id="button">Resolve</button>
<button type="button" class="btn btn-primary btn-sm" onclick="next()" id="button1" disabled="disabled">
<i class="fa fa-play"></i></button>

                             