"""
Best first search algorithms
"""

from .core import *


def greedy_search(problem: InformedProblem, fringe: GreedyQueue):
    """
    The greedy search of the problem
    :param problem: Problem containing an heuristic function
    :param fringe: Queue compatible with sorting by hint
    :return: False if there is no solution or the list of the actions (names)
    """

    fringe.insert(InformedNode(
        problem.initial_state,
        None, None, 0, 0,
        problem.heuristic(problem.initial_state)))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend_local(node, problem))


def a_star(problem: InformedProblem, fringe: GreedyQueue):
    """
     The a* search of the problem
     :param problem: Problem containing an heuristic function
     :param fringe: Queue compatible with sorting by hint
     :return: False if there is no solution or the list of the actions (names)
     """

    fringe.insert(InformedNode(
        problem.initial_state,
        None, None, 0, 0,
        problem.heuristic(problem.initial_state)))

    while True:
        if fringe.empty():
            return False

        node = fringe.remove_first()

        if problem.goal_test(node.state):
            return solution(node)

        fringe.insert_all(expend_global(node, problem))
