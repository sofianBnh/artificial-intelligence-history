"""
Knapsack resolution using simulated annealing
"""
from random import randint

from search.informed_search.neighbor_search.neighbor_search import *


class KnapsackState(ComparableState):

    def __init__(self, value: list, objects: list, max_weight: float):
        """
        :param value: list of object indexes in the data that were taken
        :param objects: list of tuple (weight, value) that we have
        :param max_weight: maximum usable weight
        """
        super().__init__(value)
        self.objects = objects
        self.max_weight = max_weight

    def get_value(self):
        """
        :returns: the overall value of the selected objects
        """
        value_count = 0
        for index in self.value:
            value_count += self.objects[index][1]
        return value_count

    def get_weight(self):
        """
        :returns: the total of the weight of the selected objects
        """
        weight_count = 0
        for index in self.value:
            weight_count += self.objects[index][0]
        return weight_count

    def quality(self) -> float:
        """
        :returns: the value of the objective function
        """
        # using the raw value as objective function
        return self.get_value()

    @staticmethod
    def is_valid_state(state) -> bool:
        """
        :param state: State to be tested
        :returns: if the weight used in the state is greater
        or equal than the maximum
        """
        return state.get_weight() <= state.max_weight

    @staticmethod
    def get_copy(state):
        """
        :param state: state to copy
        :returns: a copy of the state
        """
        return KnapsackState(copy.copy(state.value),
                             copy.deepcopy(state.objects), state.max_weight)

    def __eq__(self, state: ComparableState) -> bool:
        return self.quality() == state.quality()

    def __str__(self):
        return str(self.value)


def add_random_object(state: KnapsackState):
    # checking if there is a possible item we can add
    # getting the list of all indexes
    object_indexes_list = set(range(len(state.objects)))

    # getting the list of indexes of taken objects
    taken_object_indexes = set(state.value)

    # getting the list of indexes of the remaining objects
    available_object_indexes = object_indexes_list.difference(taken_object_indexes)
    available_object_indexes = list(available_object_indexes)

    # calculating the remaining usable weight
    weight_left = state.max_weight - state.get_weight()

    # checking if there is an object from the remaining objects that can be added
    i = 0
    impossible_add = True
    while i < len(available_object_indexes):
        if state.objects[available_object_indexes[i]][0] <= weight_left:
            impossible_add = False
        i += 1

    # if no object can be added we return the same state
    if impossible_add:
        return copy.deepcopy(state)

    # selecting a random state from the remaining states
    random_position = randint(0, len(available_object_indexes) - 1)
    added_object_index = list(available_object_indexes)[random_position]

    # creating a copy state and adding the selected object
    new_state = copy.deepcopy(state)
    new_state.value.append(added_object_index)

    return new_state


def remove_random_object(state: KnapsackState):
    # getting the list of taken objects
    taken = copy.deepcopy(state.value)

    # if no object can be removed return the same state
    if len(taken) == 0:
        return copy.deepcopy(state)

    # randomly select an object
    random_position = randint(0, len(taken) - 1)
    removed_object_index = taken[random_position]

    # creating a copy of the sate and removing the selected state
    new_state = copy.deepcopy(state)
    new_state.value.remove(removed_object_index)

    return new_state


class KnapsackProblem(LocalSearchProblem):

    def neighbour(self, state: KnapsackState) -> ComparableState:
        """
        Neighbour function that modies one item ( object ) in the state
        by adding an object or removing one
        :param state: initial state
        :returns: a new state based on the initial state with the modification
        """

        working_state = copy.deepcopy(state)

        # if no object has been selected we add an object
        if len(state.value) == 0:
            modification_method = add_random_object

        # else we randomly choose to add or remove an object
        else:
            choice = randint(0, 1)
            if choice == 0:
                modification_method = add_random_object
            else:
                modification_method = remove_random_object

        # applying the modification
        working_state = modification_method(working_state)

        # if the state isn't valid we rollback to the initial state
        # reapply the modification
        while not KnapsackState.is_valid_state(working_state):
            working_state = copy.deepcopy(state)
            working_state = modification_method(working_state)

        return working_state

    def get_random_state(self) -> ComparableState:
        # getting a copy of the initial state as a base
        current = copy.deepcopy(self.initial_state)

        current = add_random_object(current)
        before = copy.deepcopy(current)

        # keep adding objects until we can add anymore
        while KnapsackState.is_valid_state(current):
            before = copy.deepcopy(current)
            current = add_random_object(current)

        return before


class KSTemperature(Temperature):
    def update_temperature(self):
        self.current_temp = self.current_temp * 0.7


def run():
    # initialize data
    data = [
        (12, 40),
        (14, 50),
        (55, 70),
        (100, 130),
        (78, 80),
        (63, 4),
        (21, 30),
    ]

    # hyper parameters
    max_weight = 100
    max_temp = 100
    min_temp = 0.1

    # initialize objects
    initial_state = KnapsackState([], data, max_weight)

    problem = KnapsackProblem(initial_state)

    schedule = KSTemperature(max_temp, min_temp)

    # launch analyses
    result = simulated_annealing(problem, schedule)
    print("Selected Objects :", result.value)
    print("Value :", result.get_value())
    print("Used weight :", result.get_weight())


if __name__ == '__main__':
    run()
