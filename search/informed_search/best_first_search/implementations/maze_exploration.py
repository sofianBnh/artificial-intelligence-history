"""
Maze informed exploration
"""
import copy

from search.informed_search.best_first_search.best_first_search import *


# Implementing the State
class MazeState(State):

    def __init__(self, maze: list, position: tuple):
        """
        Maze state initialization
        :param maze: matrix representing the map using spaces for roads
        :param position: initial position (x,y)
        """
        self.maze = maze
        self.position = position

    @staticmethod
    def is_valid_state(state) -> bool:
        """
        :param state: state to be tested
        :returns: if the current position is valid
        """
        if 0 <= state.position[0] < len(state.maze) and 0 <= state.position[1] < len(state.maze[0]):
            return state.maze[state.position[0]][state.position[1]] == " "
        return False

    @staticmethod
    def get_copy(state):
        """
        :returns: a copy of the state
        """
        return MazeState(copy.deepcopy(state.maze),
                         copy.deepcopy(state.position))

    def __eq__(self, state) -> bool:
        return self.position == state.position

    def __str__(self):
        return "( {} , {} )".format(self.position[0], self.position[1])


# Problem Implementation
class MazeExplorationProblem(InformedProblem):

    def f_successor(self, state: MazeState) -> list:
        # possible transitions
        cases = {
            (0, 1): "East",
            (0, -1): "West",
            (1, 0): "South",
            (-1, 0): "North"
        }

        # testing each direction if the state is valid we add it to the results
        results = []
        for tuples, action in cases.items():
            current_state = move(state, *tuples)
            if MazeState.is_valid_state(current_state):
                results.append((action, current_state))

        return results

    @staticmethod
    def heuristic(state: MazeState) -> int:
        """
        Heuristic calculating an approximate distance between the
        current position and the goal position
        :param state: current state
        :returns: the mahatan distance between the state and the goal
        """
        count = 0
        for j in range(2):
            count += abs(goal_state.position[j] - state.position[j])
        return count


# Actions
def move(state: MazeState, translation_x: int, translation_y: int) -> MazeState:
    """
    Function that creates a new state with a position translated by the parameters
    :param state: initial state
    :param translation_x: translation in the X axe
    :param translation_y: translation in the Y axe
    :returns: a new state with the translated position
    """
    new_x = state.position[0] + translation_x
    new_y = state.position[1] + translation_y

    new_state = state.get_copy(state)
    new_state.position = (new_x, new_y)

    return new_state


def run():
    global goal_state
    # Creating maze
    test_maze = [
        [" ", "X", " ", " ", " ", " ", " "],
        [" ", "X", " ", "X", " ", "X", " "],
        [" ", "X", "X", " ", " ", "X", " "],
        [" ", " ", " ", " ", "X", " ", " "],
        [" ", "X", " ", " ", "X", " ", "X"]
    ]
    # Initialize the problem :

    # -  Create the initial state
    initial_state = MazeState(test_maze, (0, 0))

    # -  Create goal state function
    goal_state = MazeState(test_maze, (4, 5))

    # - Create the problem object
    problem = MazeExplorationProblem(initial_state, goal_state)

    # Run the tree search :
    result = a_star(problem, GreedyQueue())

    # Print result
    result.reverse()
    if result:
        print("The number of steps to the goal state : ", len(result))
        print()
        print("The steps are : \n")
        i = 1
        for res in result:
            print("- ", res)
            i += 1
    else:
        print("No solutions")


if __name__ == '__main__':
    run()
