# Intelligent Systems: Algorithms 


## Overview

This repo contains an implementation of the known historical search algorithms in Artificial Intelligence in its generations before the arrival of neural networks. The project documents them as a course/historical survey to help explain them with example use cases.

These algorithms usually have a *goal function* that they need to meet to consider a solution *optimal*. The first step is to generate the solution space or state space, then the algorithms go blindly or intelligently through these solutions to find the best one. In this repo, one can find the following hierarchy:

- **Uninformed Search**: Also called blind search, contains an implementation of state-space exploration.  These include `BFS`, `DFS`, `ICS`...

- **Informed Search**: Introduction to heuristics to the previous algorithms for better optimization of the search, these include: `Best First Search`, `A*`, `Generic Algorithms`...

- **Constraint Satisfaction Problems**: A special category of search algorithms that try to meet certain conditions and find a good compromise that meets the constraints. A known example of this is the *Traveling Sales Person (TSP)* problem.

## Dependencies

The algorithms only use standard libraries in python.

## Documentation

The documentation in `/docs/` was built with MkDocs. To serve it as a web page, Mkdocs needs to be installed:

```bash
sudo apt install virtualenv -y
# Get the repo with the dependancies
git clone https://sofianBnh@bitbucket.org/sofianBnh/artificial-intelligence-history.git
cd artificial-intelligence-history
virtualenv venv

# Use a local isolated version of python
source venv/bin/activate
pip install -r requirements.txt
```

To start serving the website
```bash
cd docs
mkdocs serve
```

Snapshots:

![main-docs](img/code-docs.png)
> Documented Code

![plots](img/plots.png)
> Animated Plots

![resolve](img/animated-solution.png)

> Interactive Resolution