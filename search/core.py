"""
Search Basics
"""
from abc import abstractmethod, ABC


# Exception Sub class:
class ImpossibleActionError(Exception):
    """Exception raised when the action is not allowable"""
    pass


# Default path cost function:
def basic_path_cost(state, action, result) -> int:
    return 1


# Queue structure:
class Queue(ABC):
    """Implementation of a special queue for the fringe"""

    def __init__(self):
        self.elements = []

    def empty(self) -> bool:
        return len(self.elements) == 0

    def first(self):
        return self.elements[0]

    def remove_first(self):
        element = self.first()
        del self.elements[0]
        return element

    def insert_all(self, elements: list):
        for element in elements:
            self.insert(element)

    @abstractmethod
    def insert(self, element):
        """Insertion implemented depending on the strategy"""
        pass


# State structure:
class State(ABC):
    """Model of the state of the problem"""

    # Implement if necessary
    #
    @abstractmethod
    def __eq__(self, state) -> bool:
        """
        Says is too states are the same for optimisation sake
        :param state: state to compare to
        :return: True if it's the same state
        """
        pass

    @staticmethod
    @abstractmethod
    def is_valid_state(state) -> bool:
        """
        Checks if the state is valid
        :param state: test state
        :return: True if it's compatible with the definition
        """
        pass

    @staticmethod
    @abstractmethod
    def get_copy(state):
        """
        Returns another object copied from the
        :param state: state to copy
        :return: copy of the state
        """
        pass


# Problem structure
class Problem(ABC):
    """Problem model"""

    def __init__(self, initial_state: State,
                 goal_state: State or None, path_cost=basic_path_cost):
        """
        Initialize the problem
        :param initial_state: State : the state0
        :param goal_state: State : that represents the goal
        :param path_cost : function (initial_state, action, final_state) : calculates the cost
        between two states
        """
        self.initial_state = initial_state
        self.goal_state = goal_state
        self.path_cost = path_cost

    @abstractmethod
    def f_successor(self, state: State) -> list:
        """
        :returns [ (action, state), ] depending on the actions
        needs implementation of valid and invalid cases
        """
        pass

    def goal_test(self, state: State) -> bool:
        """
        Checks if a state is the goal state
        :param state: state to test
        :return: True if it's the goal state
        """
        return state == self.goal_state


# Beans:
class Node:
    def __init__(self, state: State, parent, action: str or None,
                 depth: int or None, cost: int or None):
        self.state = state
        self.parent = parent
        self.action = action
        self.depth = depth
        self.cost = cost

    def __str__(self):
        return "{} , {}".format(self.state, self.action)


def solution(node: Node) -> list:
    """
    Function to get the solution of the problem from
    :param node: Node : last node
    :return: list : of Actions names
    """
    actions = []
    while node.parent is not None:
        actions.append(node.action)
        # actions.append(node.state.__str__())
        node = node.parent

    return actions
