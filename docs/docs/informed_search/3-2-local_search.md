# Description
<br>

Local search, is a type of search where we focus more on the solution (or an approximation of it) more than the 
sequence of actions to get there. <br>

They use in their search for the optimum solution of a problem on a neighbour function that returns a state _neighbour_
of the given state and an evaluation function decides whether to accept this solution or not. <br>

Many neighbour oriented searches were created such as **Hill Climbing** but the most efficient were the ones using 
probability in their search like **Simulated Annealing**.

<br>
---
<br>


# Simulated Annealing
<br>

Inspired by the glass annealing process, it uses a schedule function to determine the speed and the stop condition and 
an evaluation function to decide whether it will accept the solution or not. It combines the local search approach with
a random walk for better diversity. 

<br>

##State Representation
<br>

The state is the Comparable State to allow the algorithm to evaluate states from a maximisation or minimization 
perspective. It contains a value attribute which can be compared (character, number...).

```python
class ComparableState(State):
    def __init__(self, value):
        self.value = value

    @abstractmethod
    def quality(self) -> float:
        """
        :returns a mathematical amount for the state
        """
        pass

```

<br>

##Problem Representation
<br>

The problem here is represented using the _initial state_ as starting point for search and two methods:

* neighbour : Function that returns a neighbour state from the current state.

* get_random_state : Function that generates a random state.


```python
class LocalSearchProblem(ABC):
    def __init__(self, initial_state: State):
        self.initial_state = initial_state

    @abstractmethod
    def neighbour(self, state: ComparableState) -> ComparableState:
        """
        :param state: current state from which we need a neighbour
        :return: the highest value neighbor state taken from the f_successor
        function
        """
        pass

    @abstractmethod
    def get_random_state(self) -> ComparableState:
        """:returns A random state"""
        pass

```

<br>

##Temperature
<br>

The temperature is the schedule that determine the speed and the stopping condition and is also used as momentum when 
deciding if the solution is accepted. It has variable keeping track of the current temperature and an update function to
modify it according to the problem. 

```python
class Temperature(ABC):
    def __init__(self, max_temp: float, min_temp: float):
        self.max_temp = max_temp
        self.min_temp = min_temp
        self.current_temp = max_temp

    @abstractmethod
    def update_temperature(self):
        """
        Update the current temperature using some kind function
        will represent the time changes
        """
        pass

    def get_current_temperature(self) -> float:
        return self.current_temp

    def is_min_temperature(self) -> bool:
        """
        :returns: if the temperature reached the minimum limit
        """
        return self.current_temp <= self.min_temp

    def initialize_temperature(self):
        """
        Reset the temperature to it's max value
        """
        self.current_temp = self.max_temp
```

<br>

##Acceptance Function
<br>

This is the function that returns the probability of acceptance of a solution which represents the probability of having 
a better solution down the road.

```python
def acceptance_probability(old_value: float, new_value: float, 
                           temperature: float, k_factor: float) -> bool:
    """
    Probability of accepting a new value
    :param k_factor: boysman factor
    :param old_value: current best value
    :param new_value: new randomly generated value
    :param temperature: current temperature ( epoch )
    :return: if we accept the new value or not
    """
    if old_value < new_value:
        return True

    precision = 8

    evaluation = (new_value - old_value) / (temperature * k_factor)
    evaluation = 1 / 1 + evaluation

    # rounding the result for compatibility
    return math.exp(round(evaluation, precision)) > random.uniform(0, 1)

```

<br>


##Search Algorithm
<br>

The simulated annealing algorithm then starts from the initial state and keeps updating both the temperature  using the 
update function and the current state using the neighbour function until the temperature gets to its minimum, then it 
returns the current value. 

<br>

```python
def simulated_annealing(problem: LocalSearchProblem, schedule: Temperature, k_factor: float = 1):
    """
    Simulated annealing algorithm
    :param k_factor: bosyman factor
    :param problem: Local Search Problem with a neighbor function implemented
    :param schedule: a Temperature scheduler
    :returns the best solution found
    """
    current = copy.deepcopy(problem.initial_state)
    schedule.initialize_temperature()

    while not schedule.is_min_temperature():
        neighbor = problem.neighbour(current)

        if neighbor.is_valid_state(neighbor):
            if acceptance_probability(current.quality(), neighbor.quality(),
                                      schedule.get_current_temperature(), k_factor):
                current = neighbor

        schedule.update_temperature()

    return current


```

<br>
---
<br>


# Test Case
<br>

For this test case wwe will be covering a known problem in operational researches and ai, the knapsack problem in it's 
zero one variation.

<br>

**Knapsack Problem**

The concept is that we have a set of items having different weights and values, the goal is to take as much items as we 
can while respecting a maximum weight limitation and maximizing the sum of their values.

<br>

##Search Strategy
<br>

For this search we needed a neighbour function, since a state is represented as the set of objects selected (more on that
int state representation part), we can say that a neighbour state is state that has difference in an item, either we add
an item or we remove one, all of that while we can and we respect the constraint. <br> 
So to do this we created two functions :

<br>

**Adding an item** : Simply adds an item to the list of selected objects form the available objects.

```python
def add_random_object(state: KnapsackState):
    # checking if there is a possible item we can add
    # getting the list of all indexes
    object_indexes_list = set(range(len(state.objects)))

    # getting the list of indexes of taken objects
    taken_object_indexes = set(state.value)

    # getting the list of indexes of the remaining objects
    available_object_indexes = object_indexes_list.difference(taken_object_indexes)
    available_object_indexes = list(available_object_indexes)

    # calculating the remaining usable weight
    weight_left = state.max_weight - state.get_weight()

    # checking if there is an object from the remaining objects that can be added
    i = 0
    impossible_add = True
    while i < len(available_object_indexes):
        if state.objects[available_object_indexes[i]][0] <= weight_left:
            impossible_add = False
        i += 1

    # if no object can be added we return the same state
    if impossible_add:
        return copy.deepcopy(state)

    # selecting a random state from the remaining states
    random_position = randint(0, len(available_object_indexes) - 1)
    added_object_index = list(available_object_indexes)[random_position]

    # creating a copy state and adding the selected object
    new_state = copy.deepcopy(state)
    new_state.value.append(added_object_index)

    return new_state

```

<br>

**Removing an item** : Removes a randomly selected item is there is one.

```python
def remove_random_object(state: KnapsackState):
    # getting the list of taken objects
    taken = copy.deepcopy(state.value)

    # if no object can be removed return the same state
    if len(taken) == 0:
        return copy.deepcopy(state)

    # randomly select an object
    random_position = randint(0, len(taken) - 1)
    removed_object_index = taken[random_position]

    # creating a copy of the sate and removing the selected state
    new_state = copy.deepcopy(state)
    new_state.value.remove(removed_object_index)

    return new_state

```

<br>

##Code
<br>

The main code for this test case is structured as follows:

1. Initializing the object list.

2. Defining the hyper parameters (tuning parameters for the search).

3. Initialize search objects.

4. Launching the search.

```python
def run():
    # initialize object list
    data = [
        (12, 40),
        (14, 50),
        (55, 70),
        (100, 130),
        (78, 80),
        (63, 4),
        (21, 30),
    ]

    # hyper parameters
    max_weight = 100
    max_temp = 100
    min_temp = 0.1

    # initialize search objects
    initial_state = KnapsackState([], data, max_weight)

    problem = KnapsackProblem(initial_state)

    schedule = KSTemperature(max_temp, min_temp)

    # launch analyses
    result = simulated_annealing(problem, schedule)
    print("Selected Objects :", result.value)
    print("Value :", result.get_value())
    print("Used weight :", result.get_weight())
```

<br>

###State Representation 
<br>

The state is represented by the following attributes :

* Objects : list of all the objects as tuples (weight,value).

* Value : list of the selected objects (their indexes in the data list)

* Max_weight : the weight limitation.

```python
class KnapsackState(ComparableState):

    def __init__(self, value: list, objects: list, max_weight: float):
        """
        :param value: list of object indexes in the data that were taken
        :param objects: list of tuple (weight, value) that we have
        :param max_weight: maximum usable weight
        """
        super().__init__(value)
        self.objects = objects
        self.max_weight = max_weight

    def get_value(self):
        """
        :returns: the overall value of the selected objects
        """
        value_count = 0
        for index in self.value:
            value_count += self.objects[index][1]
        return value_count

    def get_weight(self):
        """
        :returns: the total of the weight of the selected objects
        """
        weight_count = 0
        for index in self.value:
            weight_count += self.objects[index][0]
        return weight_count

    def quality(self) -> float:
        """
        :returns: the value of the objective function
        """
        # using the raw value as objective function
        return self.get_value()

    @staticmethod
    def is_valid_state(state) -> bool:
        """
        :param state: State to be tested
        :returns: if the weight used in the state is greater
        or equal than the maximum
        """
        return state.get_weight() <= state.max_weight

    @staticmethod
    def get_copy(state):
        """
        :param state: state to copy
        :returns: a copy of the state
        """
        return KnapsackState(copy.copy(state.value),
                             copy.deepcopy(state.objects), state.max_weight)

    def __eq__(self, state: ComparableState) -> bool:
        return self.quality() == state.quality()

    def __str__(self):
        return str(self.value)

```

<br>

###Neighbour Function
<br>

The neighbour functions uses randomly one the two previously mentioned methods.

```python
    def neighbour(self, state: KnapsackState) -> ComparableState:
        """
        Neighbour function that modies one item ( object ) in the state
        by adding an object or removing one
        :param state: initial state
        :returns: a new state based on the initial state with the modification
        """

        working_state = copy.deepcopy(state)

        # if no object has been selected we add an object
        if len(state.value) == 0:
            modification_method = add_random_object

        # else we randomly choose to add or remove an object
        else:
            choice = randint(0, 1)
            if choice == 0:
                modification_method = add_random_object
            else:
                modification_method = remove_random_object

        # applying the modification
        working_state = modification_method(working_state)

        # if the state isn't valid we rollback to the initial state
        # reapply the modification
        while not KnapsackState.is_valid_state(working_state):
            working_state = copy.deepcopy(state)
            working_state = modification_method(working_state)

        return working_state

```

<br>

###Random State Generation
<br>


The random state is generated by randomly maxing the number of items selected and returning it.

```python
    def get_random_state(self) -> ComparableState:
        # getting a copy of the initial state as a base
        current = copy.deepcopy(self.initial_state)

        current = add_random_object(current)
        before = copy.deepcopy(current)

        # keep adding objects until we can add anymore
        while KnapsackState.is_valid_state(current):
            before = copy.deepcopy(current)
            current = add_random_object(current)

        return before

```

<br>

###Temperature
<br>

The problem isn't complicated we don't have a lot of possible combinations so the temperature value is updated using a 
big rate (it will reach its minimum faster).

```python
class KSTemperature(Temperature):
    def update_temperature(self):
        self.current_temp = self.current_temp * 0.7

```

<br>

##Results

<br>

After one thousand iteration the search returned results between **74** as a minimum and **160** as a maximum.

_Note : Values represent the overall knapsack value which is the sum of the selected object's values._

<br>

**Minimum**
<br>

Output

    Selected Objects : [5, 0, 6]
    Value : 74
    Used weight : 96

<br>

**Maximum**
<br>

Output 
    
    Selected Objects : [0, 1, 2]
    Value : 160
    Used weight : 81
  
<br>
 
**Statistics**

<br>

| Criteria             | Value      | 
|----------------------|------------| 
|Count                 |1000        |
|Mean                  |128.55      |
|Standard Deviation    |21.33       |
|Minimum               |74          |
|First Quartile  (25%) |120         |
|Median  (50%)         |130         |
|Third Quartile (75%)  |140         |
|Maximum               |160         |



<br>

**Frequency**
<br>

<br>

![bar_plot](../img/bar-plot.png)

<br>

###Hyper Parameters
<br>

In this type of search (based on probability) the choice of the parameters for the search are important, since their 
values define how good a solution is.

To demonstrate this we use the exact same algorithm with the following parameters :

     Temperature update rate =  0.9 
     Max_temp = 10000
     Min_temp = 0.00001
     
     
<br>

**Statistics**

<br>

| Criteria             | Value      | 
|----------------------|------------| 
|Count                 |1000        |
|Mean                  |136.3       |
|Standard Deviation    |16.90       |
|Minimum               |74          |
|First Quartile  (25%) |120         |
|Median  (50%)         |130         |
|Third Quartile (75%)  |150         |
|Maximum               |160         |


<br>


**Frequency**
<br>

<br>

![bar_plot](../img/bar-plot1.png)

<br>

_We notice that the changes made to the hyper parameters changed the statistics of the result, we can notice an increase 
in the number of good values and a decrease in the number of medium ones plus a decrease in the standard deviation which 
means that the results are closer to each other and are not as spread as before._