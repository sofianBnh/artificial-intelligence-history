"""
Constraint Satisfaction algorithms
"""

from .core import *


def back_tracking_search(problem: CSProblem, select_variable, select_values,
                         forward_checking_enabled: bool = False):
    """
    back tracking search with customisable selection functions
    :param forward_checking_enabled: activate or not the forward checking
    :param problem: CSP problem initialized with an empty assignment result
    :param select_variable: function for variable selection
    :param select_values: function for value selection
    :returns: None in case of a failure and a csp with assignments full else
    """
    if problem.is_complete():
        return problem.assignments

    variable = select_variable(problem)
    values = select_values(problem, variable)

    for value in values:

        if problem.is_assignment_consistent(variable, value):

            current_domain = copy.deepcopy(problem.domains)
            problem.set_assignment(variable, value)

            if forward_checking_enabled:
                forward_checking(problem, variable)

            # Recursive call
            result = back_tracking_search(problem, select_variable, select_values)

            if result is not None:
                return problem

            # Back to the previous state
            problem.remove_assignment(variable)
            problem.domains = copy.deepcopy(current_domain)

    return None
