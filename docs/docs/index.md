# Overview
 <br>

 This is a survey project about intelligent systems.
 <br> 
 The project's objectives :
 
  + Apply some of these systems on toy problems for test purposes.
 
  + Demonstrate the diffrence in performance between some of them.
 
  + Demonstrate the usage of heuristics and meta heuristics. 
 
<br>

##Author
 <br>
 
 **Name** : Benahmed Sofiane
 
 **Speciality** : SCI
 
<br>

##Module
 <br>
 
 **Module Name** : Intelligent Systems [#](https://sites.google.com/view/l3sci-si/syllabus)

 **Year** : 2017/2018

<br>

## Programming Language 
 <br>

 The Specifications of the programming language used:
 
  + **Language** : Python [#](https://www.python.org)
  
  + **Version** : 3.6.8 [#](https://www.python.org)
  
  + **Dependencies** : All dependencies used in this project are basic built-in python libraries,
   including:
    
    - **random** [#](https://docs.python.org/2/library/random.html): For generating random numbers 
      
      + randint : returns a random integer.
      
      + uniform : returns a random float.
    
    - **copy** [#](https://docs.python.org/2/library/copy.html) : 
    For getting an actual copy of an object and not a reference 
      
      + copy : returns a copy of a simple object.
      
      + deepcopy : returns a copy of a complex object.

<br>
---
<br>

# Project Division
 <br>

 The project is divided in three main parts (sub-projects) :
  
 <br>

##Uniformed Search [#](2-uninformed_search.md) 

 **Definition**
 >Blind search algorithms are search algorithms thar search a state space that use no domain knowledge 
 of the problem state. 
 They do not use heuristics. All information available to blind search algorithms is the state, 
 the successor function, the goal test and the path cost.

<br>

##Informed Search 

 **Definition**
 >Informed search algorithms are search algorithms that search a state space using some domain knowledge 
 of the problem state. 
 They use heuristics and meta heuristics. Some of theme rely on probabilities and statistics and other 
 rely on other fields like biology for Genetic Algorithms. 
 
<br>

This part is also divided to three parts :

<br>

###Best First Search [#](informed_search/3-1-bfs.md)
 
 **Definition**
 > Search guided on hard coded problem specific heuristics, including Greedy Search and A star. 
 

###Local Search [#](informed_search/3-2-local_search.md)
 
 **Definition**
 > Search guided by neighbour values, including Simulated Annealing, Hill Climbing and Local Beam Search.
 

###Genetic Search [#](informed_search/3-3-genetic_search.md)
 
 **Definition**
 > Search guided by probabilities ans quality function (fitness), inspired by genetics and the evolution theory,

<br>

##Constraint Satisfaction [#](4-csp.md)

 **Definition**
 > Constraint satisfaction problems (CSPs) are mathematical problems defined as a set of objects whose 
 state must satisfy a number of constraints or limitations. CSPs represent the entities in a problem as a 
 homogeneous collection of finite constraints over variables and specific domains, 
 which is solved by constraint satisfaction methods. In this case we can use meta heuristics.

<br>
---
<br>

#Project Architecture
 <br>

 The project is divided in sub folders one for each kind of search. All sub-projects use a common dependencies
 folder `core.py` which contains some basic classes and generic objects [#](1-core.md) .<br>
 Beside that dependence, each sub-project is layed out using this architecture:
 
      <sub-project>        
      │ 
      ├── implementation/          
      │   └── <test_case>.py         
      ├── core.py       
      └── <search_algorithm_implementation>.py      
 
 <br>
 
 Files contents:
 
 * **<_test_case_>.py** : Implementation of the test case using the specific algorithm.
 
 *  **core.py** : Dependencies, classes, abstract classes and objects used in the main algorithm.
  
 * **<_search_algorithm_implementation_>.py** : Implementation of main algorithm.

<br>
--- 
<br>

# Notes
 <br>
 
 * Every sub-project `core.py` file imports the internal dependencies needed.  
 
 * The code is documented for specific and line by line documentation.
 
 * The project documentation was generated using __Mkdocs__ with the theme _Cinder_ [#](https://github.com/chrissimpkins/cinder).

 * The plots were made using **Matplotlib** [#](https://matplotlib.org/)