var current_point = [0, 0];


var bfs_res = ["South", "South", "South", "East", "East", "East", "North", "East",
    "North", "North", "East", "East", "South", "South", "South", "West", "South"];


var dfs_res = ['South', 'South', 'South', 'East', 'East', 'South', 'East', 'North', 'North', 'East',
    'North', 'North', 'East', 'East', 'South', 'South', 'South', 'West', 'South'];


//Internal Methods

function convert(direction) {
    switch (direction) {
        case "South" :
            return [1, 0];
        case "North" :
            return [-1, 0];
        case "East" :
            return [0, 1];
        case "West" :
            return [0, -1];
    }
}

function move_to(maze, position) {
    maze[position[0]][position[1]] = "*";
    return maze;
}

function maze_to_string(maze) {
    var s = "";
    maze.forEach(function (element) {
        s += "                                     <span class=\"hljs-string\">";

        element.forEach(function (elem) {
            s += "| " + elem + " ";
        });
        s += "|</span> <br>"
    });
    return s;
}

function create_element() {

    $("#holder").remove();

    var holder = document.createElement("p");
    var parent = document.createElement("pre");
    var elem = document.createElement("code");

    holder.setAttribute("id", "holder");
    elem.setAttribute("id", "result");

    holder.appendChild(parent);
    parent.appendChild(elem);

    holder.style.display = 'none';

    $("p > #button1").after(holder);
}

function resolve() {

    $("#button").attr("disabled", "disabled").html("Resolve");
    $("#button1").removeAttr("disabled");


    var initial_point = [0, 0];

    var bfs_res_copy = ["South", "South", "South", "East", "East", "East", "North", "East",
        "North", "North", "East", "East", "South", "South", "South", "West", "South"];

    var dfs_res_copy = ['South', 'South', 'South', 'East', 'East', 'South', 'East', 'North', 'North', 'East',
        'North', 'North', 'East', 'East', 'South', 'South', 'South', 'West', 'South'];

    var maze = [
        [" ", "X", " ", " ", " ", " ", " "],
        [" ", "X", " ", "X", " ", "X", " "],
        [" ", "X", "X", " ", " ", "X", " "],
        [" ", " ", " ", " ", "X", " ", " "],
        [" ", "X", " ", " ", "X", " ", "X"]
    ];

    current_point = initial_point;

    bfs_res = bfs_res_copy;

    bfs_res = bfs_res.reverse();

    create_element();

    maze = move_to(maze, initial_point);

    $("#result").html(maze_to_string(maze));

    $("#holder").show(300);
}

function next() {

    var step = bfs_res.pop();

    if (step === undefined) {

        $("#button1").attr("disabled", "disabled");
        $("#button").removeAttr("disabled").html("Reset");
        $("#holder").hide(600);

    } else {

        var direction = convert(step);

        var maze = [
            [" ", "X", " ", " ", " ", " ", " "],
            [" ", "X", " ", "X", " ", "X", " "],
            [" ", "X", "X", " ", " ", "X", " "],
            [" ", " ", " ", " ", "X", " ", " "],
            [" ", "X", " ", " ", "X", " ", "X"]
        ];

        current_point[0] += direction[0];
        current_point[1] += direction[1];

        maze = move_to(maze, current_point);

        $("#result").html(maze_to_string(maze));
    }

}