"""
Maximize a function using genetic algorithms
"""

from search.informed_search.genetic_search.genetic_search import *


class XState(ChromosomeState):

    def __init__(self, number: int, length: int = 6):
        """
        Initializing an XState
        :param number: the x value
        :param length: size of the chromosome
        """
        number_array = XState._num_to_array(number, length)
        super().__init__(number_array)

    def get_num_value(self) -> int:
        """
        :returns: the numerical value of the chromosome
        """
        return XState._array_to_num(self.chromosome)

    # Conversion functions
    @staticmethod
    def _num_to_array(number: int, length) -> list:
        """
        Converts a number to an array of digits
        :param number: integer number to be converted
        :param length: length of the resulting array
        :returns: the array of digits
        """
        number = str(number)
        # padding
        if len(number) < length:
            number = ((length - len(number)) * "0") + number
        temp_array = list(number)

        for i in range(len(temp_array)):
            temp_array[i] = int(temp_array[i])

        return temp_array

    @staticmethod
    def _array_to_num(array: list) -> int:
        """
        Converts an array of digits to an integer
        :param array: array of digits
        :returns: the integer value
        """
        number = ""
        for elem in array:
            number += str(elem)

        return int(number)


class MaximizeFunction(GeneticProblem):

    def __init__(self, initial_state: ChromosomeState, minimum: int = 0, maximum: int = 100000):
        """
        :param initial_state: starting point of the algorithm
        :param minimum: minimum boundary of the search
        :param maximum: maximum boundary of the search
        """
        self.minimum = minimum
        self.maximum = maximum
        super().__init__(initial_state)

    def generate_random_state(self) -> ChromosomeState:
        """
        :returns: a randomly generated state from a random value
        """
        return XState(randint(self.minimum, self.maximum), self.initial_state.length)

    def get_parent_form_population(self, population: list) -> ChromosomeState:
        """
        :returns: one of the top population according to fitness
        """
        rand_position = randint(0, 10)
        return sorted(population, key=lambda member: self.fitness(member), reverse=True)[rand_position]

    def selection(self, population: list, population_size: int) -> list:
        """
        :returns: the best of the population according to fitness
        """
        return elite_selection(self, population, population_size)

    def fitness(self, state: XState) -> float:
        """
        :returns: the value of the objective function for the state
        """
        x = state.get_num_value()
        return ((- 1 / 2) * (x ** 2)) + x - 1

    def mutate(self, state: ChromosomeState) -> ChromosomeState:
        """
        :returns: the state with on of the digits of it's chromosome randomly modified
        """
        return random_single_genome_mutation(state, 0, 9, True)

    def stop_generation(self, best_fitness: list, gen_count: int):
        """
        :returns: if it reached the maximum number of generations
        """
        return generation_limitation(gen_count, 100)

    @staticmethod
    def reproduce(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
        """
        :returns: two child states generated using a random single crossing point method
        """
        children_values = single_point_crossover(parent_x, parent_y)
        # converting the values to XState objects
        children = []
        for value in children_values:
            temp_child = XState(0, parent_x.length)
            temp_child.chromosome = value
            children.append(copy.deepcopy(temp_child))

        return children


# Main function
def run():
    # Hyper parameters
    min_range = 0
    max_range = 9999
    population_size = 70
    x_array_size = len(str(max_range))
    mutation_probability = 0.01

    # Objects initialization
    initial_value = XState(min_range, x_array_size)
    problem = MaximizeFunction(initial_value, min_range, max_range)

    # Launch the search
    best: XState = genetic_search(problem, population_size, mutation_probability)

    # Printing the result
    print("Best child state : XState = ", best.chromosome)
    print("Best fitness : ", problem.fitness(best))


if __name__ == '__main__':
    run()
