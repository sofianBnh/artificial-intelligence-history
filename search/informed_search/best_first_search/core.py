"""
Dependencies for informed search
"""

from search.core import *

import operator


class InformedProblem(Problem, ABC):
    @staticmethod
    @abstractmethod
    def heuristic(state: State) -> int:
        """
        The base of the heuristic search
        :param state: State from where we need an heuristic
        :return: the hint
        """
        pass


class InformedNode(Node):
    """
    Node that contains a special attribute "hint"
    """
    def __init__(self, state: State, parent, action: str or None, depth: int,
                 cost: int, hint: int):
        super().__init__(state, parent, action, depth, cost)
        self.hint = hint


class GreedyQueue(Queue):
    """
    Queue that sorts the nodes according to the hint
    handles the repeated states
    """

    def __init__(self):
        super().__init__()
        self.visited = []

    def insert(self, element):
        if element.state not in self.visited:
            self.elements.append(element)
            self.elements.sort(key=operator.attrgetter('hint'))

    def remove_first(self):
        element = super(GreedyQueue, self).remove_first()
        self.visited.append(element.state)
        return element


# Internal methods:
def expend_local(node: InformedNode, problem: InformedProblem) -> list:
    """
    Returns the list of nodes that expend the entry node
    :param node: node to be expanded
    :param problem: data for expansion
    :return: list : of Nodes with the local hint approximation
    """
    successors = []

    for action, result in problem.f_successor(node.state):
        path_cost = node.cost + problem.path_cost(node.state, action, result)
        depth = node.depth + 1
        hint = problem.heuristic(result)
        successors.append(InformedNode(result, node, action, depth, path_cost, hint))

    return successors


def expend_global(node: InformedNode, problem: InformedProblem) -> list:
    """
    Returns the list of nodes that expend the entry node
    :param node: node to be expanded
    :param problem: data for expansion
    :return: list : of Nodes  with the global hint approximation
    """
    successors = []

    for action, result in problem.f_successor(node.state):
        path_cost = node.cost + problem.path_cost(node.state, action, result)
        depth = node.depth + 1
        hint = path_cost + problem.heuristic(result)
        successors.append(InformedNode(result, node, action, depth, path_cost, hint))

    return successors
