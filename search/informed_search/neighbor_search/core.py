"""
Dependencies for search by neighbor
"""
import math
import random

from search.core import *


class ComparableState(State):
    def __init__(self, value):
        self.value = value

    @abstractmethod
    def quality(self) -> float:
        """
        :returns a mathematical amount for the state
        """
        pass


class LocalSearchProblem(ABC):
    def __init__(self, initial_state: State):
        self.initial_state = initial_state

    @abstractmethod
    def neighbour(self, state: ComparableState) -> ComparableState:
        """
        :param state: current state from which we need a neighbour
        :return: the highest value neighbor state taken from the f_successor
        function
        """
        pass

    @abstractmethod
    def get_random_state(self) -> ComparableState:
        """:returns A random state"""
        pass


# Simulated annealing special class
class Temperature(ABC):
    def __init__(self, max_temp: float, min_temp: float):
        self.max_temp = max_temp
        self.min_temp = min_temp
        self.current_temp = max_temp

    @abstractmethod
    def update_temperature(self):
        """
        Update the current temperature using some kind function
        will represent the time changes
        """
        pass

    def get_current_temperature(self) -> float:
        return self.current_temp

    def is_min_temperature(self) -> bool:
        """
        :returns: if the temperature reached the minimum limit
        """
        return self.current_temp <= self.min_temp

    def initialize_temperature(self):
        """
        Reset the temperature to it's max value
        """
        self.current_temp = self.max_temp


# acceptance probability functions
def acceptance_probability(old_value: float, new_value: float, temperature: float, k_factor: float) -> bool:
    """
    Probability of accepting a new value
    :param k_factor: boysman factor
    :param old_value: current best value
    :param new_value: new randomly generated value
    :param temperature: current temperature ( epoch )
    :return: if we accept the new value or not
    """
    if old_value < new_value:
        return True

    precision = 8
    evaluation = (old_value - new_value) / (temperature * k_factor)
    # rounding the result for compatibility
    return math.exp(round(evaluation, precision)) > random.uniform(0, 1)
