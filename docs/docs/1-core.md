#Description
<br>

The file used here is the main `core.py` file found in the root of the the search folder.
The core files represent the dependencies used in the corresponding main algorithm and test case 
implementation. <br>  

This architecture was used to ensure the maximum re-usability of the code since most of these algorithm
use the same skeleton. <br>

This module was created as a general dependence containing the most commonly used
objects and classes making them abstract for general usage purposes.

<br>
---
<br>

#Code
<br>

The main `core.py` contains the implementation of the following classes:

* State.

* Problem.

* Queue.

* Node.

<br>

And the following functions:

* Solution.

* Basic path cost function.

<br>

##State
<br>

The state is represented as an abstract object to make as generic as possible with some abstract functions
that are used by the different search algorithms.  

```python
class State(ABC):
    """Model of the state of the problem"""

    # Implement if necessary
    #
    @abstractmethod
    def __eq__(self, state) -> bool:
        """
        Says is too states are the same for optimisation sake
        :param state: state to compare to
        :return: True if it's the same state
        """
        pass

    @staticmethod
    @abstractmethod
    def is_valid_state(state) -> bool:
        """
        Checks if the state is valid
        :param state: test state
        :return: True if it's compatible with the definition
        """
        pass

    @staticmethod
    @abstractmethod
    def get_copy(state):
        """
        Returns another object copied from the
        :param state: state to copy
        :return: copy of the state
        """
        pass

```

<br>

##Problem
<br>


The problem is represented as an abstract object that contains the following informations:

* Initial State 

* Goal State

* Path cost


```python
class Problem(ABC):
    """Problem model"""

    def __init__(self, initial_state: State,
                 goal_state: State or None, path_cost=basic_path_cost):
        """
        Initialize the problem
        :param initial_state: State : the state0
        :param goal_state: State : that represents the goal
        :param path_cost : function (initial_state, action, final_state) : calculates the cost
        between two states
        """
        self.initial_state = initial_state
        self.goal_state = goal_state
        self.path_cost = path_cost

    @abstractmethod
    def f_successor(self, state: State) -> list:
        """
        :returns [ (action, state), ] depending on the actions
        needs implementation of valid and invalid cases
        """
        pass

    def goal_test(self, state: State) -> bool:
        """
        Checks if a state is the goal state
        :param state: state to test
        :return: True if it's the goal state
        """
        return state == self.goal_state

```

<br>

##Queue
<br>

The queue is an abstract object serving as a wrapper for a list by making the insertion an abstract method
to be defined according to the strategy such as the ones used in the [Uninformed Seach](2-uninformed_search.md) 
and the [Best First Search](informed_search/3-1-bfs.md).


```python
class Queue(ABC):
    """Implementation of a special queue for the fringe"""

    def __init__(self):
        self.elements = []

    def empty(self) -> bool:
        return len(self.elements) == 0

    def first(self):
        return self.elements[0]

    def remove_first(self):
        element = self.first()
        del self.elements[0]
        return element

    def insert_all(self, elements: list):
        for element in elements:
            self.insert(element)

    @abstractmethod
    def insert(self, element):
        """Insertion implemented depending on the strategy"""
        pass

```

<br>

##Node
<br>

The node is a warping object (data structure) used for the creation of the state space.

```python
class Node:
    def __init__(self, state: State, parent, action: str or None,
                 depth: int or None, cost: int or None):
        self.state = state
        self.parent = parent
        self.action = action
        self.depth = depth
        self.cost = cost

    def __str__(self):
        return "{} , {}".format(self.state, self.action)
```

<br>

##Predefined functions
<br>

The following functions were implemented for further reuse.

<br>

**Solution function**  
Function used to retrieve the set of actions from a given node.

```python
def solution(node: Node) -> list:
    """
    Function to get the solution of the problem from
    :param node: Node : last node
    :return: list : of Actions names
    """
    actions = []
    while node.parent is not None:
        actions.append(node.action)
        # actions.append(node.state.__str__())
        node = node.parent

    return actions
```

<br>

**Basic Path cost function**  
Function used as the default path cost function where the action don't have
a specific cost.

```python
def basic_path_cost(state, action, result) -> int:
    return 1
```

