"""
Dependencies for uninformed search
"""

from search.core import *


# Queues implementations:
class BFSQueue(Queue):
    def insert(self, element):
        self.elements.append(element)


class DFSQueue(Queue):
    def insert(self, element):
        self.elements.insert(0, element)


class DLSQueue(Queue):
    def __init__(self, max_depth):
        super().__init__()
        self.max_depth = max_depth

    def insert(self, element):
        if element.depth < self.max_depth:
            self.elements.insert(0, element)


class IDSQueue(Queue):
    def __init__(self):
        super().__init__()
        self.depth = 0
        self.head = None

    def insert(self, element):
        if element.depth <= self.depth:
            if self.head is None:
                self.head = element
            self.elements.insert(0, element)

    def remove_first(self):
        elem = super().remove_first()
        if len(self.elements) == 0:
            self.elements.append(self.head)
            self.depth += 1
        return elem


class UCSQueue(Queue):
    def __init__(self):
        super().__init__()
        self.visited = []

    def insert(self, element):
        if element.state not in self.visited:
            self.elements.append(element)
            self.elements.sort(key=lambda x: x.cost)

    def remove_first(self):
        element = super(UCSQueue, self).remove_first()
        self.visited.append(element.state)
        return element


# Internal methods:
def expend(node: Node, problem: Problem) -> list:
    """
    Returns the list of nodes that expend the entry node
    :param node: node to be expanded
    :param problem: data for expansion
    :return: list : of Nodes
    """
    successors = []

    for action, result in problem.f_successor(node.state):
        path_cost = node.cost + problem.path_cost(node.state, action, result)
        depth = node.depth + 1
        successors.append(Node(result, node, action, depth, path_cost))

    return successors
