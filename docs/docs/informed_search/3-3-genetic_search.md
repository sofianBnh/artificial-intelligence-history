# Description
<br>

Genetic Algorithms are a spacial kind of local search, they also use a neighbour function to get to the next state and 
they also do not focus on finding the path for the solution but more focus on the solution it self. 

<br>

They differ from the local search algorithm whereas they use a biological approach to solving the problem, they are 
inspired from the evolution theory (also called evolutionary search algorithms) and use for that the basic principles 
such as selection, fitness and mutation, and use to generate new states a reproduction function. 

<br> 

Therefor using  inited of one state for the local search's neighbour function, to get the next state, it uses two states
in reproduction function making for example, crossing points in the chromosomes. 


<br>
---
<br>


# Genetic Algorithms
<br>

GAs, short for Genetic Algorithms, start in their search wih a set of randomly generated states called the _population_ .
This population contains a special kind of states, called here the _Chromosome States_, then in the search it tries to
reproduce these states based on their _fitness_ value and then select the new states or the members of the _new 
population_ using another selection procedure.

<br>

##State Representation
<br>

In GAs, the states are represented as an array of fixed length, which makes it easier for further manipulations.
 
```python
class ChromosomeState(ABC):
    def __init__(self, chromosome: list):
        """
        Initialize the chromosome state
        :param chromosome: array representing the state
        """
        self.chromosome = chromosome
        self.length = len(chromosome)

```

<br>


##Problem Skeleton
<br>

For GAs problem representation we define inside the problem itself the needed functions such as :

* **Fitness Function** : Used to have an estimation of how good a state is according to an objective function.

* **Selection Functions** : Used to select the _parent_ states and the _remaining population_ after each generation.

* **Reproduction Function** : Used to cross two parent states and get the newly generated states.

* **Mutation Function** : Used to make a small random modification in a state for divercity.

And also the **Stopping Condition** for the search  which indicates to the search when to stop.

<br>

```python
class GeneticProblem(ABC):
    def __init__(self, initial_state: ChromosomeState):
        self.initial_state = initial_state

    @staticmethod
    @abstractmethod
    def reproduce(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
        """
        generats children from two parent states
        :param parent_x: parent state x
        :param parent_y: parent state y
        :returns a list of new stats
        """
        pass

    @abstractmethod
    def generate_random_state(self) -> ChromosomeState:
        """
        :returns a random state
        """
        pass

    @abstractmethod
    def fitness(self, state: ChromosomeState) -> float:
        """
        :param state: state to be tested
        :returns a value for the fitness representing an objective function
        """
        pass

    @abstractmethod
    def selection(self, population: list, population_size: int) -> list:
        """
        :param population: full population of members
        :param population_size: maximum number of members
        :returns the remaining population
        """
        pass

    @abstractmethod
    def get_parent_form_population(self, population: list) -> ChromosomeState:
        """
        :param population: full population of members
        :returns a parent from the population
        """
        pass

    @abstractmethod
    def mutate(self, state: ChromosomeState) -> ChromosomeState:
        """
        :param state: member to be mutated
        :returns the member with a modification in its chromosome
        """
        pass

    @abstractmethod
    def stop_generation(self, best_fitness: list, gen_count: int):
        pass

```

<br>

###Selection 
<br>

The selection procedure is used in two cases, selecting the parents for the reproduction and the population selection 
after the completion of each generation.
 
<br>

####Parent Selection
<br>

For the parent selection it's usually based on the fitness but it can be highly problem dependent.

<br>

####Population Selection
<br>

The population selection is the process of selecting elements based on the _population_size_ using a selection strategy,
there are some known selection algorithms such as:

* Elite Selection : Selection of the best members according to their fitness.

* Wheel Selection : Probabilistic selection process giving priority to the best members according to fitness.

<br>

**Example**

```python
def elite_selection(problem: GeneticProblem, population: list, population_size: int) -> list:
    """
    :param population: full population of members
    :param population_size: maximum number of members
    :param problem: Genetic problem to get the fitness function
    :return: the best according to their fitness
    """
    population = sorted(population, key=lambda member: problem.fitness(member), reverse=True)
    return population[:population_size]

```


<br>

###Reproduction
<br>

The reproduction function is the equivalent to the neighbour function in the local search, it generates new states for 
us to test. To do this, this function takes two parent states and crosses their chromosomes to get new states.
This can be done in different ways depending on the number of crossing points, if the order maters or not.. 

<br>

**Example**

```python
def single_point_crossover(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
    """
    Performs a single point crossover from two parent states
    :param parent_x: parent state x
    :param parent_y: parent state y
    :returns: two chromosomes
    """
    # randomly selecting the crossing point
    crossing_point = randint(0, len(parent_x.chromosome) - 1)

    # calculating the new chromosomes
    value_1 = parent_x.chromosome[:crossing_point] + parent_y.chromosome[crossing_point:]
    value_2 = parent_y.chromosome[:crossing_point] + parent_x.chromosome[crossing_point:]

    return [value_1, value_2]

```

<br>


###Mutation
<br>

The mutation function is a function that slightly modifies a state to create diversity in the population, to do this we 
also have different approaches. <br>

The most common is making a random change in a genome (block in the chromosome).

<br>

**Example**

```python
def random_single_genome_mutation(state: ChromosomeState, minimum: float, maximum: float, integer: bool = False):
    """
    Function performing a mutation on a state b changing a value from the chromosome array of the state
    :param state: state to be mutated
    :param minimum: minimum of the digit value
    :param maximum: maximum of the digit value
    :param integer: if the numbers are integers
    :returns: the nex state with the mutation
    """
    length = len(state.chromosome)

    # randomly selecting the new digit
    if integer:
        new_genome = randint(minimum, maximum)
    else:
        new_genome = uniform(minimum, maximum)

    # generating the new state
    new_state = copy.deepcopy(state)
    new_state.chromosome[randint(0, length - 1)] = new_genome
    return new_state

```

<br>

###Stop Condition
<br>

For this kind of algorithms we can use different stopping conditions depending on the problem, our interests, the 
available hardware. <br>

Some known approaches : 

* **Generation Limitation** : Limiting the number of generations generated.

* **Best Fitness Stagnation** : Stopping the algorithm whenever we don't see an improvement in our best fitness.

<br>

**Example**

```python
def generation_limitation(gen_count: int, max_gen_count: int) -> bool:
    """
    :param max_gen_count: maximum number of generations
    :param gen_count: number of generations
    """
    return gen_count > max_gen_count

```

<br>

###Fitness
<br>

The fitness function is the equivalent of our objective function that we want to maximize. It will give us an estimation 
of how good a solution is.
 
<br>


##Search Algorithm
<br>

The Search algorithm is simple, it works as follows : 

* Generate a random population

* Select parents 

* Create new states from the parents

* Add them to the population

* Select the remaining members after generating a complete generation.

<br>


```python

def genetic_search(problem: GeneticProblem, population_size: int,
                      mutation_probability: float) -> ChromosomeState:
    """
    Performs a genetic search with customisable options
    :param problem: Genetic Problem
    :param population_size: number of members in a population
    :param mutation_probability: decides when to make the mutation
    :return: the best possible member
    """
    gen = 0
    population = []
    new_population = []
    best = problem.initial_state
    best_fitness = problem.fitness(best)
    elite = [best_fitness]

    for i in range(population_size):
        population.append(problem.generate_random_state())

    while not problem.stop_generation(best_fitness=elite, gen_count=gen):

        for i in range(int(population_size / 2 - 1)):
            parent_x = problem.get_parent_form_population(population)

            parent_y = problem.get_parent_form_population(population)

            children = problem.reproduce(parent_x, parent_y)

            for child in children:
                # if a random number from 0 to 1 is less than mutation probability
                if uniform(0, 1) < mutation_probability:
                    child = problem.mutate(child)

                new_population.append(child)
                current_fitness = problem.fitness(child)

                if current_fitness > best_fitness:
                    best = copy.deepcopy(child)
                    best_fitness = current_fitness
                    elite.append(best_fitness)

        population += new_population
        population = problem.selection(population, population_size)
        new_population = []
        gen += 1

    return best

```


<br>
---
<br>


# Test Case
<br>

To demonstrate how this algorithm works, we will use to find the maximum of a function. 

<br>

**Function** 
            
    f(x) = -(1 / 2) x² + x + 1 
    
<br>

**Plot**

<br>

![plot](../img/function.png)

<br>



##Code
<br>

The main code for this test case is structured as follows:

1. Hyper parameters initialization.

2. Objects initialization.

3. Launch the search.

4. Print the results

<br>

```python
def run():
    # Hyper parameters
    min_range = 0
    max_range = 9999
    population_size = 70
    x_array_size = len(str(max_range))
    mutation_probability = 0.01

    # Objects initialization
    initial_value = XState(min_range, x_array_size)
    problem = MaximizeFunction(initial_value, min_range, max_range)

    # Launch the search
    best: XState = genetic_search(problem, population_size, mutation_probability)

    # Printing the result
    print("Best child state : XState = ", best.chromosome)
    print("Best fitness : ", problem.fitness(best))

```

<br>

###State Representation
<br>

The state is represented as an array of integers that can be turned to a an integer. 

<br>

```python
class XState(ChromosomeState):

    def __init__(self, number: int, length: int = 6):
        """
        Initializing an XState
        :param number: the x value
        :param length: size of the chromosome
        """
        number_array = XState._num_to_array(number, length)
        super().__init__(number_array)

    def get_num_value(self) -> int:
        """
        :returns: the numerical value of the chromosome
        """
        return XState._array_to_num(self.chromosome)

    # Conversion functions
    @staticmethod
    def _num_to_array(number: int, length) -> list:
        """
        Converts a number to an array of digits
        :param number: integer number to be converted
        :param length: length of the resulting array
        :returns: the array of digits
        """
        number = str(number)
        # padding
        if len(number) < length:
            number = ((length - len(number)) * "0") + number
        temp_array = list(number)

        for i in range(len(temp_array)):
            temp_array[i] = int(temp_array[i])

        return temp_array

    @staticmethod
    def _array_to_num(array: list) -> int:
        """
        Converts an array of digits to an integer
        :param array: array of digits
        :returns: the integer value
        """
        number = ""
        for elem in array:
            number += str(elem)

        return int(number)

```

<br>


###Problem Representation
<br>


To represent this problem the following functions were used :

* **Parent Selection** : Randomly select one of the top ten according to fitness.
 
* **Population Selection** : we used the Elite Selection. 

* **Fitness** : we used the actual function.

* **Mutation** : we used Single Genome Mutation.

* **Reproduction** :  we used a Single Point Crossover.

* **Stopping Condition** : we used a Generation Limitation of a hundred generation.

<br>

```python
class MaximizeFunction(GeneticProblem):

    def __init__(self, initial_state: ChromosomeState, minimum: int = 0, maximum: int = 100000):
        """
        :param initial_state: starting point of the algorithm
        :param minimum: minimum boundary of the search
        :param maximum: maximum boundary of the search
        """
        self.minimum = minimum
        self.maximum = maximum
        super().__init__(initial_state)

    def generate_random_state(self) -> ChromosomeState:
        """
        :returns: a randomly generated state from a random value
        """
        return XState(randint(self.minimum, self.maximum), self.initial_state.length)

    def get_parent_form_population(self, population: list) -> ChromosomeState:
        """
        :returns: one of the top population according to fitness
        """
        rand_position = randint(0, 10)
        return sorted(population, key=lambda member: self.fitness(member), 
                      reverse=True)[rand_position]

    def selection(self, population: list, population_size: int) -> list:
        """
        :returns: the best of the population according to fitness
        """
        return elite_selection(self, population, population_size)

    def fitness(self, state: XState) -> float:
        """
        :returns: the value of the objective function for the state
        """
        x = state.get_num_value()
        return ((- 1 / 2) * (x ** 2)) + x - 1

    def mutate(self, state: ChromosomeState) -> ChromosomeState:
        """
        :returns: the state with on of the digits of it's chromosome randomly modified
        """
        return random_single_genome_mutation(state, 0, 9, True)

    def stop_generation(self, best_fitness: list, gen_count: int):
        """
        :returns: if it reached the maximum number of generations
        """
        return generation_limitation(gen_count, 100)

    @staticmethod
    def reproduce(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
        """
        :returns: two child states generated using a random single crossing point method
        """
        children_values = single_point_crossover(parent_x, parent_y)
        # converting the values to XState objects
        children = []
        for value in children_values:
            temp_child = XState(0, parent_x.length)
            temp_child.chromosome = value
            children.append(copy.deepcopy(temp_child))

        return children

```

<br>

##Results
<br>

In the end of the search the algorithm converges to the maximum displaying the results

    Best child state : XState =  [0, 0, 0, 1]
    Best fitness :  -0.5
<br>

**Plot (Animated)**

<br>

![video](../img/genetic-algo.gif)