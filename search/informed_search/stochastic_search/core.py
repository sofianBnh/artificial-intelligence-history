"""
Dependencies for stochastic algorithms
"""
import copy
from random import randint, uniform

from search.core import *


class ChromosomeState(ABC):
    def __init__(self, chromosome: list):
        """
        Initialize the chromosome state
        :param chromosome: array representing the state
        """
        self.chromosome = chromosome
        self.length = len(chromosome)


class GeneticProblem(ABC):
    def __init__(self, initial_state: ChromosomeState):
        self.initial_state = initial_state

    @staticmethod
    @abstractmethod
    def reproduce(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
        """
        generats children from two parent states
        :param parent_x: parent state x
        :param parent_y: parent state y
        :returns a list of new stats
        """
        pass

    @abstractmethod
    def generate_random_state(self) -> ChromosomeState:
        """
        :returns a random state
        """
        pass

    @abstractmethod
    def fitness(self, state: ChromosomeState) -> float:
        """
        :param state: state to be tested
        :returns a value for the fitness representing an objective function
        """
        pass

    @abstractmethod
    def selection(self, population: list, population_size: int) -> list:
        """
        :param population: full population of members
        :param population_size: maximum number of members
        :returns the remaining population
        """
        pass

    @abstractmethod
    def get_parent_form_population(self, population: list) -> ChromosomeState:
        """
        :param population: full population of members
        :returns a parent from the population
        """
        pass

    @abstractmethod
    def mutate(self, state: ChromosomeState) -> ChromosomeState:
        """
        :param state: member to be mutated
        :returns the member with a modification in its chromosome
        """
        pass

    @abstractmethod
    def stop_generation(self, best_fitness: list, gen_count: int):
        pass


# predefined methods for selection
def elite_selection(problem: GeneticProblem, population: list, population_size: int) -> list:
    """
    :param population: full population of members
    :param population_size: maximum number of members
    :param problem: Genetic problem to get the fitness function
    :return: the best according to their fitness
    """
    population = sorted(population, key=lambda member: problem.fitness(member), reverse=True)
    return population[:population_size]


# predefined methods to stop the reproduction
def generation_limitation(gen_count: int, max_gen_count: int) -> bool:
    """
    :param max_gen_count: maximum number of generations
    :param gen_count: number of generations
    """
    return gen_count > max_gen_count


def best_fitness_stagnation(best_fitness: list, threshold: float) -> bool:
    """
    :param threshold: function of decision
    :param best_fitness: list of the best elements
    :returns if the best_fitness isn't increasing compared to a threshold
    """
    top = int(len(best_fitness) / 5)

    if top == 0:
        return False

    test_population = best_fitness[:top]
    total = 0
    for i in range(top - 1):
        total += test_population[i + 1] - test_population[i]

    return total / top < threshold


# mutation function
def random_single_genome_mutation(state: ChromosomeState, minimum: float, maximum: float, integer: bool = False):
    """
    Function performing a mutation on a state b changing a value from the chromosome array of the state
    :param state: state to be mutated
    :param minimum: minimum of the digit value
    :param maximum: maximum of the digit value
    :param integer: if the numbers are integers
    :returns: the nex state with the mutation
    """
    length = len(state.chromosome)

    # randomly selecting the new digit
    if integer:
        new_genome = randint(minimum, maximum)
    else:
        new_genome = uniform(minimum, maximum)

    # generating the new state
    new_state = copy.deepcopy(state)
    new_state.chromosome[randint(0, length - 1)] = new_genome
    return new_state


# reproduction functions
def single_point_crossover(parent_x: ChromosomeState, parent_y: ChromosomeState) -> list:
    """
    Performs a single point crossover from two parent states
    :param parent_x: parent state x
    :param parent_y: parent state y
    :returns: two chromosomes
    """
    # randomly selecting the crossing point
    crossing_point = randint(0, len(parent_x.chromosome) - 1)

    # calculating the new chromosomes
    value_1 = parent_x.chromosome[:crossing_point] + parent_y.chromosome[crossing_point:]
    value_2 = parent_y.chromosome[:crossing_point] + parent_x.chromosome[crossing_point:]

    return [value_1, value_2]
