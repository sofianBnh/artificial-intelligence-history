# Description
<br>

Constraint Satisfaction Problems, or CSPs, are generic mathematical types of problems, where we have a set of variables 
with specific domains and a set of constraints on these variables. <br>

The goal then is to reach a solution that is represented by a set of assignments, each variable with a value for its 
domains, and these assignments have to coherent with the given constraints (respecting all of them). <br>

Since this is a generic type of problems many _Meta Heuristics_ have been developed to optimize this search. 


<br>
---
<br>


# Search Algorithm
<br>

Since the search in this case is trying a value for each variable and the order does not matter a DFS approach is 
preferable for space complexity sake. 

<br>


##Constraints Representation
<br>

To represent the constraints we created a holding object that is composed of two things:

* **Variables** : Set of variable names involved in this constraint.

* **Expression** : The boolean function representing the actual constraint.

<br>


```python
class Constraint:
    def __init__(self, variables: list, expression):
        """
        Representation of constraints
        :param variables: list of variable names as str
        :param expression: lambda expression returning the constraint
        """
        self.variables = variables
        self.expression = expression

```

<br>

##Problem Representation
<br>

The Problem is represented using the csp's components : 

* **Domains** : a list of accepted values for each variable.

* **Constraints** : a list of constraints.

* **Assignments** : the currently set assignments.

<br>

```python
class CSProblem:
    def __init__(self, domains: dict, constraints: list, assignments: dict = None):
        """
        Model for the CSPs
        :param domains: dictionary of variables : < "name of variable" : list of possible values>.
        :param constraints: list of Constraints.
        :param assignments: dict of assignments : < "name of variable" : value > ,
               default empty dict.
        """
        if assignments is None:
            assignments = {}

        self.domains = domains
        self.constraints = constraints
        self.assignments = assignments

    def is_complete(self) -> bool:
        """
        checks for completeness by comparing the number of assignments to
        the number of variables.
        :return: if it's complete or not
        """
        return len(self.domains) == len(self.assignments)

    def is_consistent(self, assignment) -> bool:
        """
        checks the coherence of the current assignments
        :return: if the current assignments are coherent with the conditions
        """
        for constraint in self.constraints:

            skip = False
            for var in constraint.variables:
                # If the variable isn't assigned yet don't skip the constraint
                if var not in assignment.keys():
                    skip = True

            if not skip:
                if not constraint.expression(assignment):
                    return False

        return True

    def is_assignment_consistent(self, variable: str, value) -> bool:
        """
        tests the consistent of an assignment ( variable , value )
        without saving it.

        :param variable: name of the variable
        :param value: value of variable
        :return: if the assignment is coherent or not
        """
        test_assignment = copy.deepcopy(self.assignments)
        test_assignment[variable] = value
        return self.is_consistent(test_assignment)

    def set_assignment(self, variable: str, value):
        """
        sets an assignment in the current assignments list
        :param variable: name of the variable
        :param value: value of the variable
        """
        self.assignments[variable] = value

    def remove_assignment(self, variable: str):
        """
        Removes an assignment from the current assignments
        :param variable: variable name
        """
        del self.assignments[variable]

    def __str__(self):
        return "Domains : " + str(self.domains) + " \n" \
               + "Assignments :" + str(self.assignments)

```

<br>


##Backtracking Search
<br>

The backtracking search is a recursive equivalent to a DFS based blind search using recursive calls instead of the queue
of nodes, each time it tries all values for a set variable and if the assignment is accepted it tries another variable
until either it finds a complete solution or reaches the point where there is no remaining value for all variables.


```python
def back_tracking_search(problem: CSProblem, select_variable, select_values,
                         forward_checking_enabled: bool = False):
    """
    back tracking search with customisable selection functions
    :param forward_checking_enabled: activate or not the forward checking
    :param problem: CSP problem initialized with an empty assignment result
    :param select_variable: function for variable selection
    :param select_values: function for value selection
    :returns: None in case of a failure and a csp with assignments full else
    """
    if problem.is_complete():
        return problem.assignments

    variable = select_variable(problem)
    values = select_values(problem, variable)

    for value in values:

        if problem.is_assignment_consistent(variable, value):

            current_domain = copy.deepcopy(problem.domains)
            problem.set_assignment(variable, value)

            if forward_checking_enabled:
                forward_checking(problem, variable)

            # Recursive call
            result = back_tracking_search(problem, select_variable, select_values)

            if result is not None:
                return problem

            # Back to the previous state
            problem.remove_assignment(variable)
            problem.domains = copy.deepcopy(current_domain)

    return None

```

<br>


##Meta Heuristics
<br>

Meta heuristics are non problem specific heuristics made to improve the efficiency of a CSP in detecting or avoiding the 
dead ends by improving :

* Variable Selection

* Value Selection

<br>

###Variable Selection
<br>

For variable selection we have a variety of heuristics such as :

* Ordering variables : creating an order in the variable selection.

* Minimum Remaining Values : ordering them by the number of remaining values in their domains.

* Most Constraining Variable : ordering them according to the number of constraints they have. 

<br>


**Ordering Variables**

```python
def ordered_variables(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the first alphabetically unassigned variable
    """
    possible_vars = _get_remaining_variables(problem)

    possible_vars.sort()
    return possible_vars[0]

```
<br>

**Minimum Remaining Values**

```python
def minimum_remaining_values(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the variable having the fewest possible values
    """
    variables: list = _get_remaining_variables(problem)

    mrv = variables.pop(0)
    dom = problem.domains[mrv]

    minimum_values_length = len(dom)

    for var in variables:
        # if number of remaining values in the domain are less than mvl
        if len(problem.domains[var]) < minimum_values_length:
            mrv = var

    return mrv

```

<br>

**Most Constraining Variable**

```python
def most_constraining_variable(problem: CSProblem) -> str:
    """
    :param problem: CSP with current assignments
    :returns the variable that is in the biggest number of constraints
    """
    count = {}
    unassigned = _get_remaining_variables(problem)

    # counting occurrences
    for constraint in problem.constraints:
        for var in constraint.variables:
            if var in unassigned:
                if var not in count.keys():
                    count[var] = 1
                else:
                    count[var] += 1

    # finding the max
    mcv_count = max(count.values())
    for var, counted in count.items():
        if counted == mcv_count:
            return var

```

<br>



###Value Selection
<br>

For value selection we also have a variety of heuristics such as :

* Ordering Values : order them and selecting the first.

* Least Constraining Value : returns the value that will restrict the others the least.

<br>

**Ordering Values** 

```python
def ordered_values(problem: CSProblem, variable: str) -> list:
    """
    :param problem: CSP with current assignments
    :param variable: variable name
    :returns the values for the variable ordered in an alphabetical way
    """
    problem.domains[variable].sort()
    return problem.domains[variable]

```

<br>

**Least Constraining Value**

```python
def least_constraining_value(problem: CSProblem, variable: str) -> list:
    """
    :param problem: CSP with current assignments
    :param variable: variable name
    :returns the values ordered by the number of domains affected
    """
    order = {}
    values = problem.domains[variable]
    # try
    initial_csp = copy.deepcopy(problem)
    # counting phase
    for val in values:
        problem.set_assignment(variable, val)

        order[val] = _inconsistency_count(problem, variable)

        problem = copy.deepcopy(initial_csp)

    return sorted(order, key=lambda x: order[x])

```

<br>


##Propagation Algorithms
<br>

Propagation algorithms have as purpose to propagate the inconsistency information the furthest to eliminate more the dead
ends and therefor optimizing more the search.

<br>

**Example**

```python
def forward_checking(problem: CSProblem, variable: str):
    """
    :param variable: variable lastly assigned
    :param problem: problem: CSP with current assignments
    :returns the csp modified with new domains and the list of values for the variable
    """
    new_domains = copy.deepcopy(problem.domains)

    neighbors = set(_get_neighbors(problem, variable))
    unassigned = set(_get_remaining_variables(problem))

    tested = unassigned.intersection(neighbors)

    for neighbor in tested:
        for value in new_domains[neighbor]:
            if not problem.is_assignment_consistent(neighbor, value):
                new_domains[neighbor].remove(value)

    problem.domains = new_domains

```


<br>
---
<br>



# Test Case
<br>

For this algorithm we chose the __Graph Coloration Problem__ which consists in assigning a color to each node of the 
graph a color with the constraint that none of the related nodes have the same color.

<br>

**Constraint Graph**

<br>

A CSP can be represented as _constraint graph_ showing each variable and its neighbour variables :
 
<br>

* **Nodes** : represents variables.
 
* **Links** :  represents a constraint between them.

<br>

**Tested Problem** 

<br>

We have four variables with the inequality constraint linked as shown here. 

<br>

![graph](img/csp_graph_before.png)

<br>

With the domain **Red**, **Blue**, **Yellow**, for each variable.

<br>


##Code 
<br>

The main code for this test case is structured as follows:

1. Set the domains.

2. Initialize the variables and their domains.

3. Set the constraints.

4. Initialization of the problem.

4. Launch the search.

5. Print the results.

```python
def run():
    # Available colors
    colors = ["red", "blue", "yellow"]

    # Variables and domains definition
    domains = {
        "x1": copy.deepcopy(colors),
        "x2": copy.deepcopy(colors),
        "x3": copy.deepcopy(colors),
        "x4": copy.deepcopy(colors)
    }

    # Constraints
    constraints = [
        Constraint(["x1", "x2"], lambda assignments: assignments["x1"] != assignments["x2"]),
        Constraint(["x1", "x3"], lambda assignments: assignments["x1"] != assignments["x3"]),
        Constraint(["x2", "x3"], lambda assignments: assignments["x2"] != assignments["x3"]),
        Constraint(["x2", "x4"], lambda assignments: assignments["x2"] != assignments["x4"]),
    ]

    # Initialization of the problem
    problem = CSProblem(domains, constraints)

    # Launch backtracking search
    back_tracking_search(problem, most_constraining_variable, least_constraining_value, True)

    # Print results
    print("Result assignments :")
    for assignment, value in problem.assignments.items():
        print("- {} = {} ".format(assignment, value))

```

<br>

##Results

<br>

In the end of the search the algorithm outputs the results : 

        Result assignments :
        - x2 = red 
        - x1 = blue 
        - x3 = yellow 
        - x4 = blue 
        

<br>

**Graph** 

<br>

![result_graph](img/csp_graph_after.png)
