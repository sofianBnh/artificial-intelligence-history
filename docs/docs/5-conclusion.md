#Conclusion
<br>

The algorithms and techniques covered in this documentation were the first phase of AI, or the AI 1.0.

<br>

The new wave of algorithms are machine learning based algorithms and use more of the probabilistic and 
statistical background for example :

<br>

* **Support Vector Machines** for supervised classification tasks.

* **K-means Clustering** for unsupervised classification. 

* **Linear and Logistic Regression**  for prediction.


<br>

And the currently outstanding algorithms are the ones based on the _Neural Networks_ and _Deep Learning_ including the 
most recent advances in it like **Auto Encoders** and **Generative Adversarial Networks**.